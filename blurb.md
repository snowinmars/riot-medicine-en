_Riot Medicine_ is a comprehensive guide to autonomous street medicine during protest and civil unrest.
Defending your community from fascists and challenging the State can be dangerous.
Comrades put their bodies on the line for the causes they believe in, and when injured it is the responsibility of riot medics to help mend them.

_Riot Medicine_ covers organizing, medicine, equipment, and tactics.
This resource is designed for both licensed physicians taking to the streets and experienced protesters taking on a medical role.
Step-by-step instructions and discussion of urban legends will help the reader effectively deliver evidence-based treatments.

Struggles for liberation will always include injuries, but with some first-aid knowledge, you can minimize this harm.
