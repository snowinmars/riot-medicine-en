\chapter{Psychological Care}
\label{ch:psychological_care}
\index{psychology|(}

\epigraph{
The victim who is able to articulate the situation of the victim has ceased to be a victim: he or she has become a threat.
}{James Baldwin, \textit{The Devil Finds Work}\supercite{devil-finds-work}}

\noindent
Treating patients goes beyond bandaging a wound or wrapping patients in a blanket and moving on.
Providing care includes offering emotional support for the individuals who have been traumatized.
Many injuries received while fighting fascists and the State are minor and leave little more than scrapes and bruises, but the lasting damage can be the sense of violation of one's own body or autonomy or the shock of experiencing injustice.
This trauma often extends to people who haven't been injured themselves but have witnessed others being traumatized such as those present during instances of police brutality.
Watching a comrade get abused can be emotionally taxing, and liberals who still believe in so-called ``law and order'' may be horrified by the repression protesters face.

Stress and psychological trauma may not only be directly related to what is occurring during an action.
Individuals may have other stressors in their life, and an overt reaction may be due to built up stress breaking through, or something about the action may be a trigger for their past psychological trauma.
This may be compounded by the fact that an action may not exist in isolation.
For months on end, there may be weekly or even daily actions during widespread unrest.

\section*{Physiology}

The stress response is a physiological phenomenon that leads to changes in the body, so familiarity with the various systems involved in experiencing stress and fear are useful for understanding how we respond to various stressors.\footnotemark[1]

\footnotetext[1]{
Understanding the underlying mechanisms of stress helps in treating patients.
It's too easy to say ``they were freaking out,'' and knowledge of these mechanisms is one way to avoid this pitfall.
Compassion and experience are other means to this end.
I cannot, however, teach compassion and give you experience via this book, so this chapter leans more into the biological bases of stress.
}

\subsection*{Nervous System}
\index{nervous system!physiology@physiology of|(}
\index{nervous system!peripheral|(}

The human nervous system is divided into the central nervous system\index{nervous system!central} and the peripheral nervous system.
The central nervous system consists of the brain and the spinal cord, and the peripheral nervous system consists of all other nerves and ganglia.

\index{nervous system!autonomous|(}

The peripheral nervous system is divided into the somatic nervous system (voluntary)\index{nervous system!somatic} and the autonomic nervous system (involuntary).\footnotemark[2]
The somatic nervous system consists of sensory neurons and motor neurons, and it is responsible for the transmission of information to the brain, voluntary control of the skeletal muscles, and reflex arcs\index{reflexes} in skeletal muscles.
The autonomic nervous system consists of the nerves that control smooth muscle, glands, and internal organs.

\footnotetext[2]{
Additionally, there is the enteric nervous system\index{nervous system!enteric} which controls the functions of the gastrointestinal tract.\index{gastrointestinal tract}
}

\index{fight or flight response|see {nervous system, sympathetic}}
\index{nervous system!sympathetic|(}
\index{nervous system!parasympathetic|(}

The autonomic nervous system is divided into the sympathetic and parasympathetic nervous systems.
The two systems act in a complementary opposition to one another (antagonistic control).
The sympathetic nervous system is often likened to the accelerator and the parasympathetic nervous system to the brakes.
The sympathetic nervous system is responsible for the functions of the body that prime it for action and quick responses, in particular the ``fight or flight'' response.
The parasympathetic nervous system is responsible for the functions of the body associated with resting, often simplified to ``rest and digest'' and ``feed and breed.''
Some of the specific effects of these two systems can be seen in \autoref{tab:autonomic_effects}.

\begin{table}[htbp]
\footnotesize
\caption[Effects of the Autonomic Nervous System]{Effects of the Autonomic Nervous System\supercite[pp. 360--361]{human-physiology-ch11}}
\label{tab:autonomic_effects}
\centering

\begin{tabularx}{\linewidth}{l|X|X|}
    \multicolumn{1}{c|}{\textbf{Organ}} &
		\multicolumn{1}{c|}{\textbf{Sympathetic Effects}} &
		\multicolumn{1}{c|}{\textbf{Parasympathetic Effects}} \\
    \hline
    Heart & Increased heart rate and contractility & Decreased heart rate and contractility \\
    \hline
    Lungs & Dilation of bronchioles & Constriction of bronchioles \\
    \hline
    Blood vessels & Dilation in skeletal muscle, constriction in \acrshort{GI} tract & --- \\
    \hline
    Sweat glands & Activation of sweat glands & --- \\
    \hline
    Eye & Pupil dilation & Pupil constriction \\
    \hline
    Liver & Increased glucose release & Increased glycogen production \\
    \hline
    Adipose tissue & Fat breakdown & --- \\
    \hline
    Stomach / intestines & Decreased motility & Increased motility \\
    \hline
    Bladder & Retention of urine & Release of urine \\
    \hline
    Penis / testes & Ejaculation & Erection \\
    \hline
    Uterus & Contraction & Engorgement and secretions \\
    \hline
\end{tabularx}
\end{table}

\index{nervous system!parasympathetic|)}
\index{nervous system!sympathetic|)}
\index{nervous system!autonomous|)}
\index{nervous system!peripheral|)}
\index{nervous system!physiology@physiology of|)}

\subsection*{The Stress Response}
\index{stress response!physiology@physiology of|(}

The human stress response activates systems in the body in order to prepare it for action as a means of survival.
The stress response involves feedback loops between different ``systems.''
These systems include information received from sensory organs, cognitive appraisal of the stressor, neurological triggering mechanisms, the neuroendocrinological response, activation of the target organs, and our ability to cope.\supercite[pp. 28--9]{treatment-stress-response}
The stress response may begin at a sub-emotional level (``gut feeling'') and eventually become a full-blown emotional response to a stressor.
It is important to note that the autonomic nervous system's output is influenced by emotions generated in the cerebral cortex\index{cerebral cortex!stress response@in stress response} and limbic systems\index{limbic system!stress response@in stress response}.\supercite[pp. 357]{human-physiology-ch11}\textsuperscript{,}\footnotemark[3]
Simple examples of this include blushing from embarrassment, ``butterflies in one's stomach'' when they see their crush, or hair standing on end (piloerection) as a result of fear.
Feedback from the stress response can further alter cognitive appraisal and the emotional response.
This can perpetuate the body's stress response.\supercite[pp. 45]{treatment-stress-response}

\footnotetext[3]{
Of the effect perception has on the stress response, Hans Selye famously said, ``It is not what happens to you that matters, but how you take it.''\supercite{the-stress-of-life}
}

\index{adrenaline|see {epinephrine}}

\index{amygdala!stress response@in stress response|(}
When someone senses a threat, their eyes and ears send information directly to their amygdala.
The amygdala is a part of the limbic system which, among other functions, processes emotional responses and helps form memories.
The amygdala sends signals to the hypothalamus\index{hypothalamus!stress response@in stress response} which sends signals to the adrenal glands\index{adrenal glands!stress response@in stress response} which release epinephrine\index{epinephrine!stress response@in stress response} (adrenaline).\footnotemark[4]
The individual experiences increased heart rate, blood pressure, and respiratory rate; dilated bronchioles; sharpened senses; and a release of stored glucose\index{glucose}.\supercite{understanding-stress-response}
\index{amygdala!stress response@in stress response|)}

\footnotetext[4]{
\index{urban legends!epinephrine@in epinephrine}
\index{urban legends!adrenal glands@in adrenal glands}
\index{urban legends!cortisol@in cortisol}
\index{adrenal glands!urban legend@in urban legends}
\index{cortisol!urban legends@in urban legends}
\index{epinephrine!urban legends@in urban legends}
It should be noted that adrenal fatigue, the idea that sustained stress can cause adrenal glands to produce insufficient levels of hormones like cortisol and epinephrine, does not exist.\supercite{adrenal-fatigue, adrenal-fatigue-allergy}
Adrenal fatigue is pseudoscience.
This urban legend is briefly noted here because it is something that has occasionally been mentioned by medics and activists.
}

The impulses that begin in the sensory neurons likewise project into the neocortex\index{neocortex!stress response@in stress response} causing an increase in muscle tone\index{muscular tone!stress response@in stress response} and at times even complete motor activity.\supercite[pp. 32]{treatment-stress-response}
This cascade from external stimulation to the bodily reaction can be faster than humans can perceive.
If you've ever jumped out of the way of danger before you realized what you were doing, you surely understand this.

\begin{figure}[htbp]
\centering
\caption[Sabo-Tabby Displaying an Acute Stress Response]{Sabo-Tabby Displaying an Acute Stress Response\footnotemark[5]}
\includesvg[height=4cm, keepaspectratio]{sabo-tabby}
\end{figure}

\footnotetext[5]{
The Sabo-Tabby, or by its more canonical name ``The Black Cat,'' is a symbol associated with the \acrshort{IWW}\index{Industrial Workers of the World}, anarcho-syndicalism, direct action, and (most specifically) sabotage.
}

\index{hypothalamic-pituitary-adrenal axis|(}
\index{cortisol|(}
If the body still perceives a threat after this initial surge of epinephrine, the body keeps the sympathetic nervous system activated via a hormonal cascade taking place via the hypothalamus, pituitary gland, and the adrenal glands.
These feedback interactions are known as the \acrlong{HPA} axis (\acrshort{HPA} axis).
The activation of the \acrshort{HPA} axis causes the release of the hormone cortisol\index{cortisol}\footnotemark[6] from the adrenal glands.
Cortisol increases blood glucose by generating glucose\index{glucose} from non-carbohydrate sources (gluconeogenesis) and by breaking down glycogen stores (glycogenolysis), and it counteracts insulin\index{insulin!stress response@in stress response} which can lead to hyperglycemia\index{hyperglycemia!stress response@in stress response}.
Cortisol also suppresses the release of inflammation causing substances as well as generally suppresses the immune system\index{immune system!stress response@in stress response} due to its high metabolic demand.
Other endocrine axes are activated including the adrenal-cortical axis\index{adrenal-cortical axis} which releases endorphins\index{endorphins!stress response@in stress response} which increase the individual's pain threshold\index{pain!perception@perception of}.
When the threat is no longer perceived, the \acrshort{HPA} axis returns to normal activity, and cortisol levels fall.
The parasympathetic nervous system dampens the stress response.
\index{cortisol|)}
\index{hypothalamic-pituitary-adrenal axis|)}

\footnotetext[6]{
Cortisol is not only a hormone that is released in response to a stressor.
It also helps regulate bodily functions, and its release into the body follows a diurnal cycle.
}

\index{stress response!physiology@physiology of|)}

\section*{Stress and Psychological Trauma}
\index{stress!traumatic|see {psychological trauma}}
\index{psychological trauma|seealso {stress}}
\index{trauma!psychological|see {psychological trauma}}

\index{psychological trauma|(}
\index{stress|(}

Stress can be broadly described as a real or perceived threat to homeostasis\index{homeostasis!stress@in stress}.\supercite[p. 342]{vanders}
This can be fear, injury, disease, or even simple things like exposure to the cold.
Stress, with or without lasting psychological trauma, leads to the physiological changes discussed in the previous section.

Stress can also manifest from non-psychological causes.
Exposure to the cold, blood loss, and the consumption of caffeine are examples of biogenic stressors.
Absent appraisal by the individual, they still elicit a stress response from the body.\supercite[p. 8]{treatment-stress-response}

It should also be noted that not all stress is bad or harmful.
Some amount of stress is beneficial and can lead to motivation, and a total lack of stress can make life feel boring or dull.
Additionally, perception of a stressor as positive can lead to growth and joy.

\index{stress|)}

\subsection*{Psychological Trauma}

The \acrshort{DSM-5}\index{DSM-5} defines a traumatic stressor as ``any event (or events) that may cause or threaten death, serious injury, or sexual violence to an individual, a close family member, or a close friend.''\supercite[p. 830]{dsm-5}
This definition must be expanded upon to include witnessing such events, even when those harmed are unknown, and it must remove the caregiver's subjective assessment of the severity of the stressor.\footnotemark[7]
Some people may live through brutal violence relatively unscathed, but what may seem like a minor stressor can still cause great distress and trauma to others.\supercite{stress-and-psychiatry}

\footnotetext[7]{
This isn't just my opinion.
Psychiatrists and other professionals agree.\supercite[pp. 548]{treatment-stress-response}
}

In a social movement based on solidarity, one must consider violence happening to unknown comrades as a traumatic stressor.
The slogan ``An injury to one is an injury to all'' dates back to the founding of the anarcho-syndicalist \acrlong{IWW} (\acrshort{IWW})\index{Industrial Workers of the World} in 1905,\supercite{haywood-bio} and it persists over a century later in their current constitution\supercite{iww-preamble} as well as among other forms of labor organizing.
Most importantly, it is used as a cry for solidarity in the anarchist and antifascist movements, and it captures the sense of harm that comes from watching local and distant attacks against our communities.
The feeling is something deeply internalized, and many anarchists will feel gut-wrenching agony when watching the suffering of someone they will never know.
There is evidence that viewing horrors on media\supercite{disaster-media} and content moderation on social media\supercite{youtube-moderation-ptsd} are stressors that can lead to \acrshort{PTSD}\index{post-traumatic stress disorder}.
Anecdotally most all of us know someone who has had to take a break from social media and reading the news to avoid gulping down the deluge of global human rights abuses.

\begin{figure}[tb]
\centering
% https://www.1001freedownloads.com/free-clipart/an-injury-to-one-is-an-injury-to-all
\includesvg[width=\textwidth, keepaspectratio]{injury-to-one}
\end{figure}

Psychological trauma can be both acute and chronic, the former being associated with a single event or events happening over a short period of time and the latter being associated with long-term exposure to traumatic stressors.
With all this in mind, the definition of a traumatic stressor provided by the Jane Addams Collective\index{Jane Addams Collective} is more applicable for medics as it is more general:

\begin{displayquote}
Trauma can be defined as a psychologically significant/impactful event that creates a rupture in a person’s sense of self, worldview, or view of the future.\supercite{mutual-aid-trauma}
\end{displayquote}

Some specific examples of traumatic stressors are experiencing or witnessing any of the following:\supercite{dsm-5, psych-first-aid-field-guide, mutual-aid-trauma}

\begin{itemize}
    \item Death (both human and non-human) through accident, negligence, or malice
    \item Natural or human-made disasters
    \item Physical assault and abuse
    \item Arrest, abduction, disappearance, or imprisonment
    \item Injury or illness, particularly with grotesque or dramatic characteristics
    \item Sexual assault and abuse
    \item Emotional abuse
    \item Marginalization, invalidation, and dismissal
\end{itemize}

\index{post-traumatic stress disorder|(}

Exposure to traumatic stressors can lead to \acrlong{ASD} (\acrshort{ASD}\index{acute stress disorder}) or \acrlong{PTSD} (\acrshort{PTSD})\index{post-traumatic stress disorder}.
Failure to treat \acrshort{ASD} (a short lived set of symptoms) can lead to \acrshort{PTSD} (a long lived set of symptoms).\footnotemark[8]
Differentiating between these two is outside the scope of this book.
The following clusters of symptoms and behaviors generally characterize PTSD and exposure to traumatic stressors.

\footnotetext[8]{
This is somewhat of a simplification, but it follows the \acrshort{DSM-5}'s criteria, which suffices for this book.
}

\index{intrusive thoughts!post-traumatic stress disorder@in post-traumatic stress disorder|(}
\triplesubsection{Intrusion symptoms}
Individuals may have intrusive memories and nightmares of the traumatic event.
They may experience flashbacks and feel as though they are reliving the event and may disassociate to the point where they are unaware of their present surroundings.
\index{intrusive thoughts!post-traumatic stress disorder@in post-traumatic stress disorder|)}

\index{disassociation!post-traumatic stress disorder@in post-traumatic stress disorder|(}
\triplesubsection{Dissociative symptoms}
Individuals may have an altered sense of reality or sense of disembodiment.
They may have amnesia\index{amnesia!post-traumatic stress disorder@in post-traumatic stress disorder} and be unable to fully remember the traumatic event.
Note that these symptoms are different than disassociation during flashbacks.
\index{disassociation!post-traumatic stress disorder@in post-traumatic stress disorder|)}

\triplesubsection{Avoidance symptoms}
Individuals may actively avoid thinking about the traumatic event and other things that remind them of the traumatic event.
They may also avoid people and places that remind them of the traumatic event.

\triplesubsection{Arousal symptoms}
Arousal symptoms may be mild and only include sleep disturbances or sudden outbursts (both verbal and physical) directed at others.
They may also include more severe symptoms like hyperarousal\index{hyperarousal}, hypervigilance\index{hypervigilance}, and an exaggerated startle response.
Individuals may be perpetually on the lookout for threats in normally non-threatening situations.

\triplesubsection{Negative mood}
Individuals may also have a generally negative mood and find it difficult to experience positive emotions.
They may have persistent feelings of anger, guilt, or shame.
They may have persistent negative beliefs about themself (``I am worthless'') and of the world (``Everyone is bad'').
They may feel estranged from others and have reduced participation in their normal activities.

\index{post-traumatic stress disorder|)}
\index{psychological trauma|)}

\subsection*{Psychological First Aid}
\index{psychological first aid|(}

Psychological first aid (\acrshort{PFA}) is a framework for providing supportive, emotional care to individuals who have experienced traumatic events.
It is not some exact set of steps, and you should adapt it to the cultural and social norms of your region.
Additionally, \acrshort{PFA}'s background is in disaster response (like a hurricane, earthquake, or large terror attack), and as such it assumes that providers of \acrshort{PFA} are dealing with a \acrlong{MCI} (\acrshort{MCI})\index{mass casualty incidents}.
In most scenarios where medics are in service, the number of individuals that will need \acrshort{PFA} at a given time is relatively low, and the individuals in need of care tend to be easily identifiable.
Since medics typically do not operate in conjunction with governments or large \acrshort{NGO}s, this book modifies the traditional \acrshort{PFA} steps to fit the smaller scale and more autonomous nature of riot medicine.

The major goals of \acrshort{PFA} include:

\begin{itemize}
    \item Helping individuals feel safe
    \item Avoiding retraumatizing or additionally traumatizing individuals
    \item Connecting individuals to other individuals and groups who can provide further aid
    \item Restoring a sense of autonomy
\end{itemize}

Like how medical first aid is the initial step into managing a wound, \acrshort{PFA} is the initial step in the process of recovery from trauma.
Judith Herman described this process as follows:

\begin{displayquote}
The fundamental stages of recovery are establishing safety, reconstructing the trauma story, and restoring the connection between survivors and their community.\supercite[p. 3]{trauma-and-recovery}
\end{displayquote}

Long-term recovery from trauma it is not something that falls under the umbrella of \acrshort{PFA} and riot medicine.
Medics are encouraged to get involved with other collectives that offer psychological care, though that is outside the scope of this book.
Such specialist resources for psychological care may already exist in your community, and you may be able to connect patients to them.
Activist Trauma Support\index{Activist Trauma Support}\supercite{activist-trauma-support} previously operated in the UK\index{United Kingdom},
QueerCare\index{QueerCare}\supercite{queercare} currently operates in the UK,
and Out of Action\index{Out of Action}\supercite{out-of-action} currently operates in Germany\index{Germany}.
For further reading, see \textit{Mutual Aid, Trauma, \& Resiliency} by the Jane Addams Collective\index{Jane Addams Collective}.

It should also be noted that \acrshort{PFA} is not the same as \acrlong{PD} (\acrshort{PD})\index{psychological debriefing}\index{debriefing!psychological}.
\acrshort{PD} generally involves discussion of the traumatic event with emphasis on finding out what the individual thinks happened and explaining what actually happened.
PD is not effective in reducing PTSD and may cause harm, and thus it is not recommended.\supercite{effective-treatment-ptsd, psych-first-aid}
\acrshort{PFA} aims to avoid retraumatizing the individual, something that may happen if they are made to discuss the traumatic event.

\subsubsection*{\acrshort{PFA} Steps}
\index{psychological first aid!steps|(}

\acrshort{PFA} uses some some combination of the following steps.

\index{medics!self-preservation|(}
\triplesubsection{Ensure your own safety}
Like with other first aid, you need to ensure your own safety.
If police are still actively terrorizing individuals, you may not be able to intervene and begin care.
If fascists have made an incursion into your action and there is street fighting, you may not be able to help until the violence has died down.
\index{medics!self-preservation|)}

\index{triage!psychological first aid@in psychological first aid|(}
\triplesubsection{Identify who needs care}
Identify who needs care and quickly triage them.
Individuals with moderate and severe physical injuries should take priority over individuals who have been traumatized.
The basics of \acrshort{PFA} is kindness and calmness which is something most people can provide, but you as a medic may be one of a small number of people who can offer medical care.
Individuals who have been on the receiving end of violence may not always be the ones who need care as they may have expected said violence and used proactive coping\index{coping} measures to prepare for it.
Look for individuals who seem frozen, dazed, or panicked.
They are the ones more likely to need \acrshort{PFA}.
\index{triage!psychological first aid@in psychological first aid|)}

\index{consent|(}
\triplesubsection{Introduce yourself and obtain consent}
Like with first aid, identify yourself and obtain consent to treat the individual.
State your qualifications, what you are able to offer, and ask if you can assist them.
\index{consent|)}

\triplesubsection{Remove the patient from the source of trauma}
If the patient is still in a chaotic environment or near where a traumatic incident took place, you may not be able to care for them or help calm them.
Consider moving them somewhere that is calmer and has a greater sense of security than their current location.
This may not be possible, or you may need to stay close to the area where the traumatic event occurred so that others may be able to find you for medical care.

\triplesubsection{Separate the calm from the panicked}
If you have multiple patients, you may need to separate the calm-but-traumatized from the panicked patients.
The patients who are panicked and highly animated may erode the small amount of calmness in other patients.

\index{privacy!patients@of patients|(}
\index{anonymity!patients@of patients|(}
\triplesubsection{Keep nosy busybodies away from patients}
Bystanders may be curious about what happened and ask invasive questions to the patients.
This may further traumatize them as it may force them to speak about the traumatic event, and it can interfere with their ability to feel calm and safe.
Journalists\index{journalists!patient privacy@in patient privacy} may try to take photos or ask questions to get a scoop.
Keep busybodies and journalists away from patients.
\index{anonymity!patients@of patients|)}
\index{privacy!patients@of patients|)}

\triplesubsection{Help the patient feel safe, comfortable, and calm}
Help the patient feel safe.
Moving them to a safe location is a good first step.
Telling them that you are here to help and that others are there for support can be helpful.
Offer them something to drink or a snack.
If they are crying, offer them tissues.
While quite simple, this is a powerful gesture that symbolizes both care and a return to normalcy.
Speak to them in a calm and reassuring voice, both in tone and content.

\quadruplesubsection{Keep the focus on the patient}
Ask the patient about how they feel, and avoid making your interaction about how you feel.
Keep your judgement about the event and your appraisal of their response out your treatment.
It does not matter if you think their reaction to the traumatic event is appropriate or not.
Don't tell them they're overreacting.
All feelings are valid.
You are there to provide care and support regardless of your interpretation of the event.

\quadruplesubsection{Do not lie or misrepresent the situation}
While you attempt to calm and reassure the patient, do not lie about resources available or the current situation.
If there are unknowns, you may state so, and you may choose to omit some details if they are not relevant or helpful.
However, do not make statements like ``everyone is fine'' or ``nothing bad is going to happen to you now'' unless you know with certainty that such statements are true.

\triplesubsection{Be mindful of how you communicate}
Communication isn't just words alone but tone, facial expressions, and posture.
Try to see yourself from the patient's eyes considering your relation to them both culturally as well as through your relative privileges.
Maintain a pleasant disposition.
Face your body towards them, and keep your focus on them.
If they are sitting, either sit or squat so you do not loom above them.
When they talk, nod your head so they can see that you are actively listening.

\triplesubsection{Consider offering physical contact}
Some patients may find physical contact to be helpful and calming.
This may mean a hand on their shoulder, holding their hands, or a hug.
Ask for content before touching a patient, and be explicit about what kind of physical contact you are going to offer.
For example, clearly say, ``would you like a hug?''

\triplesubsection{Prompt the patient to speak}
Ask the patient if they would like to speak about what happened.
Do not force them to discuss anything as this may retraumatize them.
Allow for long pauses as they collect their thoughts.
Repeatedly asking them questions can add to the stress of the situation and interrupt their train of thought.
One exception to this is if there is still an active threat and they were one of a small number of witnesses.
If there is still danger (like an armed assailant), you may need to get them to answer basic details to help protect others.

\index{grounding techniques|(}
\triplesubsection{Use grounding techniques}
The patient may be panicked or disassociated\index{disassociation!psychological trauma@in psychological trauma} from the current situation.
Encourage them to breathe slowly.
Help them identify 5 things they are feeling (physically) and 5 things they can see or hear that are calming.
Ask them what they see in the clouds, what the weather is like, or if they can hear any birds.
Have them put their feet firmly on the floor or their hands in the lap, and ask them to describe the feeling.
\index{grounding techniques|)}

\index{autonomy!personal|(}
\triplesubsection{Enable self sufficiency}
Once you have attended to the patient's immediate needs like safety and minor material comforts, ask them what they need.
Do they need to get home?
Do they have friends at the action who can help them?
Are they worried about someone else at the action, and can you help them find this person?
Do they want to be connected with more qualified providers of psychological care?
Facilitate their own autonomy.
\index{autonomy!personal|)}

\quadruplesubsection{Create a plan}
Help the patient come up with a short-term plan they can immediately enact.
For example, make a plan to get them home.
Discuss this with them, and then act on it.

\triplesubsection{Connect them to support}
Help connect the patient to support in their life such as friends, family, or other organizations that work with activists.
Ask them what they have done to cope in the past, and suggest they try using these measures again.
If you know of support networks for activists or traumatized individuals, provide them with business cards or a means of contacting this additional care.
Even something as simple as a left-leaning community kitchen where they can meet others may have some benefit.

\index{coping|(}
\triplesubsection{Suggest and encourage positive coping}
Your care may be brief, but the patient may need ongoing care.
Some of this care can be done on their own, and you can encourage healthy coping strategies such as:

\begin{itemize}
    \item Getting enough sleep
    \item Remembering to eat and drink enough water
    \item Staying connected with and talking to friends, family, and comrades
    \item Finding someone with whom they can talk about their trauma
    \item Spending time with their pets
    \item Continuing to engage in fun and relaxing activities like game night, reading, or seeing live music
    \item Exercising and spending time outdoors, even if it's as simple as walking around their neighborhood
\end{itemize}
\index{coping|)}

\index{patient discharge|(}
\triplesubsection{Discharging your patient}
You do not want your patient to feel abandoned.
You may have to care for others, and if you need to leave your patient temporarily, tell them so, and tell them you will be back.
If you need to end care because they are leaving or you need to leave, ensure they have been connected to someone or another group that can continue to care for them.
This connection should not be vague and abstract, but concrete.
Introduce them to someone, and ensure that the next provider is aware that you are transferring care.
Of course, no such provider may be available, and you do not have unlimited time to spend with each patient.
You may not be able to give a proper hand-off, and you may have to attend to your own needs.
Do what you can to make the termination of care not feel abrupt.
\index{patient discharge|)}

\index{psychological first aid|)}
\index{psychological first aid!steps|)}

\section*{Your Acute Stress Response}
\index{medics!stress@and stress|(}

In addition to understanding how to provide psychological first aid to others, medics should know how to regulate their responses to acute stressors.

The belief that you can overcome obstacles reduces the amount of stress you experience.
Such a belief can be cultivated through studying, practice, and experience\index{coping}.
Experience with protests, and in particular having a sense of what will or will not happen, can prevent, or at least minimize, acute stress responses to police movements and attacks.

Secondary to preparation, the stress response can be reduced as it begins via a conscious effort to be calm and to relax.
Perhaps in the past you've felt the trickle of stress hormones when you see a line of riot cops don their helmets in preparation for a charge.
Use of the grounding techniques, something often practiced by individuals who have \acrshort{PTSD} or experience panic attacks, can help you give order to the world and reduce stress.

There are many grounding techniques, though not all are appropriate for use during an action.\footnotemark[9]
Practices that are meditative in nature are helpful.
Slow, deep, rhythmic breathing while thinking calming thoughts may help.
Describe to yourself what you see and feel.

\footnotetext[9]{
For example, closing your eyes and visualizing a peaceful scene is likely more dangerous than helpful.
}

\begin{itemize}
    \item Can you feel the ground beneath your feet?
          Is it firm or soft, wet or dry?
    \item Is there wind?
          Which direction is it coming from?
          How does it feel on your face?
    \item How many cops do you see?
          What are they carrying?
    \item How many protesters are there?
          What are they doing?
    \item How does your backpack feel?
          Is it heavy or light?
          Tight or loose?
    \item What is in your pockets?
          Can you feel these items pressing against your body?
          What do they feel like?
    \item What is the weather like?
          Are you warm or cold?
          Do you have goosebumps, or are you sweating?
\end{itemize}

Being able to describe the world around you helps ground you in reality and move the focus away from the stressor.
Answering such questions also helps you appraise the tactical situation.

These techniques, and in particular these questions, may not be helpful for you.
You may find focusing on the police worsens your stress.
Discover what works for you through conscious experimentation.

Further, in your service as a medic, you may experience what is known as ``compassion fatigue''\index{compassion fatigue} or secondary \acrshort{PTSD}\index{post-traumatic stress disorder!secondary} which results from caring for others who have been traumatized and injured.\supercite{psych-issues-rescue}
You should take note of the state of your mental health both while at actions and over longer periods of time to ensure that you are healthy enough to continue providing aid to your community.

\index{medics!stress@and stress|)}

\section*{Summary}

As it has been said before, and as it will be repeated throughout this book, one of the ways a medic can help heal patients is by being a beacon of calm amidst chaos and panic.
This calm can be used for organizing other medics and guiding treatment.
Furthermore, it is a critical component of psychological first aid.
Remember that calmness and composure do not mean that a medic needs to be stoic and lacking in outward compassion.
\acrshort{PFA} involves making the patient feel safe and calm, attending to their basic needs, avoiding retraumatizing them, and connecting them to additional means of care.
Help the patient come up with a short-term plan they can act on to help them restore their autonomy.

\index{psychology|)}

% TODO reading
% - https://sci-hub.tw/10.1016/B978-0-12-411512-5.00018-X
% - https://sci-hub.tw/10.1016/S0950-351X(87)80063-0
% - https://sci-hub.tw/10.1016/S0005-7967(01)00133-4
% - https://sci-hub.tw/10.1017/S1092852900001954

% TODO refs
% - https://outofaction.blackblogs.org/
% - https://www.activist-trauma.net/

% exercise helps prevent stress?
%   - https://sci-hub.tw/10.1016/j.psyneuen.2008.08.023
%   - https://sci-hub.tw/10.1016/j.psyneuen.2007.04.005
%   - https://sci-hub.tw/10.1111/j.1469-8986.2006.00373.x

% Judi Chamberlin's (1978) On Our Own: Patient Controlled Alternatives to the Mental Health System

% TODO treatment of human stress chapters: 3, 4, 5 (parts), 8, 9, 10, 11, 12?, 13?, 14?, 15, 16, 19, 21?, 22?, 23?, 24?, 25, 26, 27
