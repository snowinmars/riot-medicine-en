\chapter{Fractures and Dislocations}
\label{ch:fractures_and_dislocations}

\epigraph{Therefore, the subject is not whether we accomplish Anarchism today, tomorrow, or within ten centuries, but that we walk towards Anarchism today, tomorrow, and always.}{Errico Malatesta, \textit{Towards Anarchism}\supercite{towards-anarchism}}
\noindent

Fractures and dislocations are less common during actions than simple wounds or contamination with riot control agents.
They require significantly more force than cuts and bruises, so they are less likely to occur unless the intent is to maim rather than simply to hurt or use pain compliance.
They may be the result of police brutality, hand-to-hand combat, being hit by a vehicle, or even just accidents such as falling from a height.
Most injuries of this nature are non-lethal, so being able to identify and immobilize fractures and dislocations will help minimize the amount of pain a protester has to endure before they can reach definitive treatment.

\section*{Physiology}
\index{bones!physiology@physiology of|(}

The adult human skeleton consists of 206 bones whose functions include providing support for the body, facilitating movement, and protecting delicate organs.
The skeleton is divided into the axial skeleton consisting of the skull, vertebral column, ribs, and sternum.
The remaining bones make up the appendicular skeleton consisting of the shoulder girdle, pelvis, arms, and legs.

% TODO need to mention marrow
Bones are not solid.
They consist of a hard outer layer (coritcal bone) that gives bones their smooth shape and provide the rigidity that allows bones to carry out their primary functions of support, movement, and protection.
Inside the bones are canals that run parallel and perpendicular to the bone that allow blood vessels and nerves to reach inner tissues.
Spongy bone (cancellous bone) is less dense and weaker but more flexible than cortical bone.
It's functions include providing load distribution in long bones and producing platelets, red blood cells, and white blood cells.

\index{bones!physiology@physiology of|)}

\begin{figure}[htbp]
\centering
\caption{Bone Cross-Section\supercite{baedr}}
\includesvg[width=\textwidth, height=4cm, keepaspectratio]{bone-cross-section}
\end{figure}

\index{joints|seealso {dislocations}}
\index{joints!physiology@physiology of|(}
A joint is the connection between two or more bones.
There are multiple types of joint when classified by function.
Fibrous joints are fused, inflexible joints like between the bones of the neurocranium.
Cartilaginous relatively immovable joints that joined by cartilage like the joint between the left and right pubic bones (pubic symphysis).
Facet joints exist between the articular processes in adjacent vertebra, and they limit and guide motion in the spinal column.
Synovial joints (\autoref{fig:synovial_joint}) are the most common kind of joint in the human body.
They are a fluid filled cavity surrounded by a fibrous capsule that allows free range of motion.
Examples of these are the wrist, shoulder, and knee.
\index{joints!physiology@physiology of|)}

\begin{figure}[htbp]
\centering
\caption{Synovial Joint\supercite{citriii}}
\label{fig:synovial_joint}
\includesvg[height=6cm, width=\textwidth, keepaspectratio]{synovial-joint}
\end{figure}

\index{ligaments!physiology@physiology of|(}
Ligaments are connective tissue that connects bone to bone.
Ligaments can both guide and prevent movement.
Ligaments are viscoelastic.
They strain gradually under load but return back to their original shape when the load is removed.
This is elasticity is not unlimited.
Beyond a certain point they cannot return to their original shape leading to joint instability.
\index{ligaments!physiology@physiology of|)}

\index{tendons!physiology@physiology of|(}
Tendons, also known as sinews, are connective tissue that usually connect a muscle to bone.
Like ligaments, tendons can stretch under tension.
Muscle contractions pull on the tendon which moves the bone it is connected to.
\index{tendons!physiology@physiology of|)}

\section*{Fractures}
\index{bones!injury@injury to|see {fractures}}
\index{fractures|(}

A fracture is a break in the continuity of a bone.
When a bone fractures, there may be edema or hematoma cause by ruptured bone marrow and damaged blood vessels.
Blood vessels may become pinched, blocking blocking blood flow to an extremity.

Fractures can be classified based on their origin.
Traumatic fractures are caused by direct impact or other sudden forces.
Pathological fractures are caused as a result of weakening of bones due to disease such as osteoporosis\index{osteoporosis}, though they may still be caused by comparatively small forces.
Pathological fractures from osteoporosis may occur in the patients who are elderly, are alcoholic\index{alcoholism!fractures@in fractures}, or have anorexia nervosa\index{anorexia nervosa!fractures@in fractures}.

Fractures can be classified as open or closed depending on whether or not they may be contaminated by the environment.
A closed fracture is a fracture with no break in the skin.
An open fracture is a fracture may have bone protruding from skin or it may have an open wound that communicates with the fracture or surrounding hematoma.

Fractures can be classified as displaced or non-displaced depending on whether the fragments have shifted relative to each other.
Not all fracture result in a change in shape of the bone or extremity.

\begin{figure}[htbp]
\caption{Fracture Types\supercite{anon-3}}
\centering
\begin{subfigure}[b]{4.5cm}
    \centering
	\includesvg[width=\textwidth, height=7cm, keepaspectratio]{fracture-closed-non-displaced}
	\caption{Closed/Non-Displaced}
\end{subfigure}
\begin{subfigure}[b]{4.5cm}
    \centering
	\includesvg[width=\textwidth, height=7cm, keepaspectratio]{fracture-closed-displaced}
	\caption{Closed/Displaced}
\end{subfigure}
\begin{subfigure}[b]{4.5cm}
    \centering
	\includesvg[width=\textwidth, height=7cm, keepaspectratio]{fracture-open-non-displaced}
	\caption{Open/Non-Displaced}
\end{subfigure}
\begin{subfigure}[b]{4.5cm}
    \centering
	\includesvg[width=\textwidth, height=7cm, keepaspectratio]{fracture-open-displaced}
	\caption{Open/Displaced}
\end{subfigure}
\end{figure}

When a bone fractures, there can be significant loss of circulating blood.
Estimations of these values for a closed fracture are listed in \autoref{tab:fracture_blood_loss}.
These values can be double in open fractures.
Class III hemorrhages (1500 to \SI{2000}{\ml}) are approximately when hypovolemic shock occurs\index{shock!hypovolemic!fractures@in fractures}.
Hemorrhage classifications are discussed in \fullref{ch:wound_management}.

\begin{table}[htbp]
\caption{Estimated Blood Loss from Fractures}
\label{tab:fracture_blood_loss}
\centering
\begin{tabular}{l|l}
    \multicolumn{1}{c|}{\textbf{Bone}} & \multicolumn{1}{c}{\textbf{Blood Loss (ml)}} \\
    \hline
    Rib             & 125                          \\
    Radius or ulna  & 250--500                     \\
    Humerus         & 750                          \\
    Tibia or fibula & 500--1000                    \\
    Femur           & 1000--2000                   \\
    Pelvis          & $> 1000$, typically $> 2000$ \\
\end{tabular}
\end{table}

% TODO risk of smoking for fractures (bone density, see wiki for a link https://en.wikipedia.org/wiki/Bone_fracture)

% TODO stress fracture

\index{fractures|)}

\section*{Dislocations}
\index{dislocations|(}

A dislocation is the abnormal separation of the bones of a joint (\autoref{fig:shoulder_dislocation}).
Dislocations are typically caused by trauma, though some individuals may have congenital predispositions to dislocations and may experience dislocations with only minor force.
Dislocations can cause damage to the tissue around the joint such as ligaments, tendons, nerves, and blood vessels.

\begin{figure}[htbp]
\centering
\caption{Shoulder Dislocation\supercite{anon-3}}
\label{fig:shoulder_dislocation}
\includesvg[height=6cm, keepaspectratio]{shoulder-dislocation}
\end{figure}

\index{dislocations|)}

\section*{Compartment Syndrome}
\index{compartment syndrome|(}

Compartment syndrome is increased pressure within a fascial space.
This increased tissue pressure eventually leads to \gls{ischemia}.
Compartment syndrome is a complication of injuries such a fractures, crush injuries, and burns, though it may also be caused by repetitive strain on a muscle or tight bandages.

The main signs and symptoms are worsening pain and pain that is out or proportion with the original injury.
Pain is exacerbated by passive stretching of the affected muscles.
Other signs include pallor or cyanosis of the tissue surrounding and \gls{distal} to the compartment.
Distal pulse may be absent.
The patient may complain of a feeling of pressure, and the compartment may feel tense when palpated.

Use the 5 P's of critical limb ischemia\index{ischemia} to help identify compartment syndrome: pain, pallor, paraesthesia, paralysis, and pulselessness.

\begin{table}[htbp]
\caption{The 5 P's of Critical Limb Ischemia}
\centering
\begin{tabular}[c]{l}
    \hline
    Pain          \\
    Pallor        \\
    Paraesthesia  \\
    Paralysis     \\
    Pulselessness \\
\end{tabular}
\end{table}

Compartment syndrome is a medical emergency for which there is no non-surgical treatment.
The patient must be evacuated to advanced medical care.

\index{compartment syndrome|)}

\index{fractures|(}
\index{dislocations|(}

\section*{Signs and Symptoms of Fractures and Dislocations}

Signs and symptoms of fractures and dislocations are similar, and in many cases there is little utility in attempting to make a differential diagnosis between the two in the field.
Dislocations may have associated fractures.
In most cases, medical imaging is needed to confirm the diagnosis and guide treatment.

Signs and symptoms include pain and tenderness.
There may be discoloration, either bruising or redness.
There may be swelling or deformity of the appendage or joint.
There may be \gls{crepitus}\index{crepitus} (the sound of bone rubbing).
An extremity may be moving in a way that seems unnatural.
There may be stiffness, loss of range of motion, and loss of strength.
The \gls{distal} pulse may be weak or absent due to pinched or damaged blood vessels.

\section*{Assessment}

Assessment of a fracture or dislocation is done to differentiate a body part with a fracture or dislocation from a body part that is merely bruised, strained, or sprained.
If you suspect a head or spinal fracture, you need to immobilize the patient and take additional precautions.
If you suspect a rib fracture, you need to check for complications and damage to the heart and lungs.
See \fullref{ch:brain_and_spinal_cord_injuries} and \fullref{ch:chest_injuries} for more information.

\triplesubsection{Consider \acrshort{MOI}}
Assess the \acrlong{MOI} to determine if there may have been sufficient force for cause a fracture or dislocation.
If the \acrshort{MOI} does not seem to indicate dislocation but you suspect it, ask the patient if they have hypermobility or have dislocated that joint before.

\triplesubsection{Check for deformity}
Expose the injured body part and check for deformity.
Humans are bilaterally symmetrical, so the opposite body part may be used for comparison.
Start by doing a visual comparison.
This may be insufficient due to bruising such as in cases of police brutality.
You may need to feel for deformity.
Do so gently at first as this may cause significant pain.

\triplesubsection{Check extremity for \acrshort{CSM}}
Check the extremity for circulation, sensation, and motion to determine if there may be a pinched or severed blood vessel.
Skin should be warm and pink.
Check for a pulse distal to the injury.
Cyanosis, cool skin, or lack of a pulse suggest pinched or damaged blood vessels.
This is an emergency, and the patient requires immediate evacuation to advanced medical care.

\index{neurapraxia!transient|(}
Note that police are taught to target nerve groups with their batons to cause transient neurapraxia.
This is a temporary loss of strength and sensation while the nerve is stunned.
This disability typically lasts under 5 minutes, so sensation and motion may need to be rechecked to confirm your diagnosis of a fracture or dislocation.
\index{neurapraxia!transient|)}

\section*{Treatment}

Treatment involves immobilizing the affected body part and evacuating the patient.
Definitive treatment for fractures and dislocations should generally be done by a medical professional in a clinical setting.
Attempting to set a bone or dislocation yourself can further damage tissue.
Failure to properly set a bone can lead to malunion or nonunion.
However, there are some cases where treatment in the field is appropriate.

\triplesubsection{Remove tight clothing and jewelry}
Tight clothing and jewelry can constrict blood vessels if there is swelling around the fracture or dislocation.
Remove jewelry and clothing.
Depending on the nature of the injury, clothing may need to be cut away.

\triplesubsection{Clean and dress wounds}
Wounds need to be cleaned and dressed prior to splinting.
Exposed bone should be irrigated, but do not scrubbed.
Wrap exposed bone with gauze soaked in water or saline.
Tightly wrapping wounds should be avoided as swelling may cause constriction.

\triplesubsection{Consider in-line traction}
In-line traction can reduce pain during splinting and make transport easier.
Consider applying in-line traction or reducing the fracture or dislocation before splinting the injury.
In-line traction and reduction are covered in detail later in this chapter.

\index{splinting|(}

\triplesubsection{Immobilize the injury}
Immobilize the fracture or dislocation before moving the patient.
If the patient is being evacuated by ambulance, and the arrival is expected to be prompt, significant immobilization efforts may be unnecessary.

Splint the bones above and below a dislocated joint.
Splint the joints above and below a fractured bone.
If you cannot differentiate between a fracture or dislocation and a less serious injury like a sprain or strain, it is preferable to splint to prevent additional pain and tissue damage.
Splinting techniques are covered later in this chapter.

\index{splinting|)}

\triplesubsection{Check CSM}
After splinting, and periodically during evacuation, check the patient's CSM distal to the splint.
Swelling may impair circulation, and dressings or splints may need to be loosened.
Consider if impaired CSM is due to compartment syndrome.

\triplesubsection{Treat for shock}
Patients with fractures and dislocations may go into shock due to pain or internal bleeding.
This is especially true for fractures of the femur and pelvis.

\index{PRICE method|(}
\triplesubsection{Use the PRICE method}
Protection, rest, ice, compression, and elevation (\acrshort{PRICE}) can reduce pain and help prevent swelling.
Generally, evacuation is done quickly, so there is neither time nor space for this treatment.
During prolonged evacuations from remote actions, this may be advisable.
\acrshort{PRICE} is covered in more detail in \fullref{ch:athletic_injuries}.
\index{PRICE method|)}

\triplesubsection{Consider evacuation}
Depending on the nature of the action, even minor fractures and dislocation that you are able to treat can pose risk to the patient, so patients may need to be send home.
Patients with fractures to the femur or pelvis, with multiple fractures, with degraded of absent CSM, or who may be in shock should be immediately evacuated to advanced medical care.

\subsection*{In-Line Traction}
\index{in-line traction|(}

First aid courses often teach to immobilize a fracture or dislocation as it was found to prevent damage to tissues.
Riot medicine takes after wilderness medicine where there is consensus that gently applying traction and attempting to straighten a fracture or dislocation can reduce pain and make splints more stable.\supercite[p. 77]{nols}
In-line traction should be applied for femur fractures, fractures that are difficult to splint due to angulation, and fractures or dislocations that impair distal CSM.

In-line traction for fractures is best applied by two medics, though it can be done by one (\autoref{fig:in_line_traction}).
The first medic stabilizes the limb above the fracture, and the second medic grips the limb below the fracture.
The second medic gently pulls the limb along the long axis of the bone and attempts to alight the bone ends.
When there is resistance or the patient experiences pain, stop and splint the limb in its current position.

\begin{figure}[htbp]
\centering
\caption{In-Line Traction\supercite{baedr}}
\label{fig:in_line_traction}
\includesvg[width=\textwidth, height=5cm, keepaspectratio]{in-line-traction}
\end{figure}

\subsubsection*{Reducing Dislocations}
\index{reduction|(}

Treatment for a dislocation is called reduction.\footnotemark[1]
Dislocations should only be reduced if transport to advanced medical care will take hours or if circulation is impaired and prompt evacuation is not possible.
Reasons to consider reduction in the field is that it reduces pain and swelling and makes transport easier.
Most actions being urban and suburban means that advanced medical care is close by, eliminating the need to reduce dislocations in the field.

\footnotetext[1]{\textit{re} (``back [to initial position]'') + \textit{ducere} (``lead/bring'')}

In-line traction can also be used to reduce certain types of dislocations.
Dislocations of the \gls{anterior} shoulder,\footnotemark[2] patella (kneecap), finger, or toe can be reduced with in-line traction.
Other dislocations such as the wrist, ankle, hip, knee, and elbow should not be treated in the field unless there is impaired CSM or evacuations in excess of several hours.
These are more complex, and there is risk of harm if medics attempt reduction.

\footnotetext[2]{The shoulder ``popping out'' to the front}

In all cases, medics should use discretion when considering field reduction of dislocations.

\triplesubsection{Reducing shoulder dislocations}
Shoulder dislocations can be identified by shoulder deformity or a ``drooping'' shoulder.
An \gls{anterior} shoulder dislocation can be reduced by having the patient lie face down on an elevated surface with their arm hanging freely (\autoref{fig:passive_shoulder_traction}).\footnotemark[3]
Attach a 2 to \SI{5}{\kilogram} weight to their hand or manually apply a very small amount of traction to the patient's arm to help relax their muscles.
Maintain this traction until their shoulder pops back into place.

\footnotetext[3]{This is also known as the Stimson technique.}

\begin{figure}[htbp]
\centering
\caption{Passive Shoulder Traction}
\label{fig:passive_shoulder_traction}
\includesvg[height=6cm, keepaspectratio]{passive-shoulder-traction}
\end{figure}

\index{reduction|)}
\index{in-line traction|)}

\subsection*{Splinting}
\index{splinting|(}

The primary function of splinting is to immobilize, support, and protect a fracture or dislocation.
A good splint pads the injury and allows medics to checks distal CSM during evacuation.
The support for a splint can be a commercial foam splint, piece of cardboard, piece of a foam sleeping pad, or other improvised adjuncts.

Foam splints can be molded to the shape of the extremity.
Other splints should be padded with gauze or strips of cloth to conform to the shape of the body.
Splints are held in place using triangle bandages, gauze rolls, self-adhering bandages, webbing, or tape.

\begin{figure}[htbp]
\centering
\caption{Splinting an Arm\supercite{baedr}}
\includesvg[height=4cm, keepaspectratio]{splinting-an-arm}
\end{figure}

\subsubsection*{Techniques}

The following are techniques for treating specific fractures and dislocations.

\triplesubsection{Fingers}
Fingers can be splinted individually using commercial or improvised splints.
Another technique is buddy taping where the injured finger is taped to an adjacent finger for support (\autoref{fig:buddy_tape_finger}).
When buddy taping, place gauze between the fingers to prevent chaffing and \gls{maceration}\index{maceration!fractures@in fractures}.

\triplesubsection{Toes}
Toes are buddy taped in much the same way fingers are (\autoref{fig:buddy_tape_toe}).
Additionally, user a long strip to tape the toes to the top of the foot to provide support.
This is especially important if the pinky toe is injured.
Because the toes are small, you may need to cut your strips of tape in half lengthwise.

\begin{figure}[htbp]
\caption{Buddy Taping\supercite{baedr}}
\centering
\begin{subfigure}[b]{4cm}
    \centering
	\includesvg[width=\textwidth, height=4cm, keepaspectratio]{buddy-taping-finger}
	\caption{Finger}
    \label{fig:buddy_tape_finger}
\end{subfigure}
\begin{subfigure}[b]{4cm}
    \centering
	\includesvg[width=\textwidth, height=4cm, keepaspectratio]{buddy-taping-toe}
    \caption{Toe}
    \label{fig:buddy_tape_toe}
\end{subfigure}
\end{figure}

\triplesubsection{Hand}
Injuries to the hand that are not limited to the fingers require splinting the wrist.
Splint the hand in the position of function.
This is slightly open as when the hand hangs freely when at one's side.

\index{collarbone|see {clavicle}}
\triplesubsection{Arm and shoulder}
Slinging and swathing is used to immobilize the arm and shoulder (\autoref{fig:sling_and_swathe}).
This is particularly useful for clavicle\index{clavicle} fractures.

\quadruplesubsection{Sling}
A sling can be made using a triangle bandage.
Hold the triangle bandage up to their chest so that the longest side is vertical and opposite the side with the fracture.
Have the patient place their hand on their breast.
Pull the top corner around the back of their neck and tie it to the bottom corner.
The third corner should be approximately at their elbow with enough extra  material to make a flap.
This flap will cup their elbow and prevent their arm from sliding out of the sling.
Fold this flap around their elbow and use safety pins to secure the flap.
Alternatively, use a long strip of duct tape to secure the flap.

\quadruplesubsection{Swathe}
Use a second triangle bandage, strips of cloth, or webbing to secure the arm to the patient's torso.
Injuries to the elbow may make it impossible to sling and swathe, so you may need to improvise immobilization.

\begin{figure}[htbp]
\centering
\caption{Sling and Swathe\supercite{baedr}}
\label{fig:sling_and_swathe}
\includesvg[width=\textwidth, height=8cm, keepaspectratio]{sling-and-swathe}
\end{figure}

\triplesubsection{Pelvis}
A fracture pelvis is a medical emergency.
Treat a fractured pelvis like a spinal injury.
Immobilize the torso and legs, and treat for shock.
To immobilize the pelvis, tie a jacket around the patient's pelvis, and hold it in place using webbing or multiple interlocked belts.\footnotemark[4]

\footnotetext[4]{
Commercial pelvis slings are too large, heavy, and expensive to make sense for medics to carry.
}

\triplesubsection{Hip and femur}
Fractures and dislocations of the hip and femur can cause painful muscle contractions.
The entire leg and pelvis need to be immobilized with a splint.
Improvising a traction splint it not recommended.\footnotemark[5]
They are difficult to improvise, have contraindications for certain types of fractures, and are not well supported by medical evidence.\supercite[p.84]{nols}

\footnotetext[5]{
\index{splints!traction|(}
A traction splint is a device that is anchored above and below the site of the fracture and allows constant traction to be applied to the fracture.
A traction splint for a femur anchors to the upper thigh or pelvis.
The ankle is strapped to a rod that extends from the anchor.
A winch is tightened to apply traction to the lower leg.
Clearly it can be seen that such a device is too large to carry at most actions and that it is difficult to improvise one that functions adequately.
\index{splints!traction|)}
}

\triplesubsection{Knee}
Minor dislocations of the knee can be supported by an elastic bandage.
With assistance, a patients may be able to walk and assist in their own evacuation.
For more severe dislocations, splint the patient's leg carry them out.

\triplesubsection{Ankle}
Fractures and dislocations of the ankle may be indistinguishable from a sprain.
The patient's ankle may be splinted or wrapped according to the steps in \fullref{ch:athletic_injuries}.

\index{splinting|)}
\index{fractures|)}
\index{dislocations|)}

\section*{Summary}

Fractures and dislocations can be serious medical conditions that require treatment by advanced medical care to avoid permanent disability.
Treatment is typically immobilization and evacuation, though some minor injuries may be treated in the field.
Serious fractures may lead to hypovolemia due to internal bleeding, so patients need to be treated for shock.
Fractures and dislocations can be confused with sprains and strains, so when in doubt, splint the extremity and evacuate.
