\chapter{Unresponsive States}

\epigraph{Because a government won't grant us all freedom /
The rich won't give back the land /
The only true freedom is freedom for all /
Each life guided by its own hand
}{Dawn Ray'd, \textit{Black Cloth}\supercite{dawn-rayd-thorn-blight}}

\noindent
During actions, individuals may become unresponsive for a variety of reasons.
This chapter covers unresponsive states that aren't associated with other illness and injuries covered by the previous chapters.

\section*{Seizures}
\index{seizures|(}

A seizure is a period of disruption to normal brain function caused by a sudden surge of inappropriate electrical activity in the brain.
Not all seizures are epileptic as seizures have diverse causes.
Epilepsy\index{epilepsy} is a condition where a patient has recurrent epileptic seizures.

A primary (idiopathic) seizure is one with no identifiable cause.
A secondary (symptomatic) seizure is one with an identifiable neurological cause such as a previous traumatic brain injury or stroke.
A reactive seizure is one with causes such as trauma, electrical stimulation, and metabolic disturbances in individuals without epilepsy.
Reactive seizures are self correcting and are not classified as epileptic seizures.\supercite[p. 1173]{tintinallis}

Causes of seizure include hypoglycemia, traumatic brain injury, heat stroke, use of recreational drugs, sleep deprivation, and some types of insect stings.
Stress and flashing lights can cause seizures in individuals with epilepsy.
Despite taking anticonvulsants, individuals with epilepsy may also have breakthrough seizures.
These can be more dangerous as they may be more unexpected.

\subsection*{Signs and Symptoms}

Signs and symptoms depend on the type of seizure a patient experiences.

\triplesubsection{Partial seizure}
Partial (focal) seizures are caused by electrical activity in one part of the brain, and they may spread to other parts or the whole brain.
Signs and symptoms include repetitive behavior (automatism) such as simple gesturing, fiddling, lip smacking, or repetitive speech or incoherent vocalizations; sensory hallucinations; significant changes in mood including paranoia, depression, and ecstasy; and distorted memory or perception.
The patient remains conscious and may be able to continue whatever activities they were engaged in, and they may have amnesia\index{amnesia!seizures@in seizures} and not remember the seizure.
The sensory hallucinations are known as auras and may precede generalized seizures.

\triplesubsection{Generalized seizure}
Generalized seizures are suspected to caused by simultaneous electrical activity in the entire cerebral cortex.

A generalized tonic-clonic seizure (grand mal) is what most most people think of when they think of a seizure.
During the tonic phase, the patient becomes rigid and falls to the ground.
This phase subsides and the patient's movements go from coarse to smaller, rhythmic (clonic) trembling.
Patients may urinate or vomit, and they may be apneic leading to hypoxia which can lead to cyanosis.
Typically an attack lasts 60 to 90 seconds followed by rapid, deep breathing and unconsciousness.
The patient's consciousness gradually returns and is followed by a state of fatigue and confusion that may last for hours.

An absence seizure (petit mal) is a brief seizure lasting only several seconds.
The patient develops an altered state of consciousness giving the perception that they are confused or withdrawn.
They may retain normal posture or have some amount of slumping or jerking.
Their current activity ceases (e.g., they stop walking or talking), and their eyes may start twitching.
They may urinate, and they may not respond to voice or touch stimuli.
The seizure ends abruptly and the patient will continue with their activity without realizing anything has happened.
Observers may describe an absence seizure as ``spacing out'' if they are unfamiliar with the symptoms.

\index{status epilepticus|(}
\triplesubsection{Status epilepticus}
Status epilepticus is a generalized seizure lasting for 5 or more minutes or two consecutive seizures without regaining consciousness.
\index{status epilepticus|)}

\subsection*{Treatment}

You cannot stop a seizure that is in progress, but you can prevent secondary injury.

\triplesubsection{Protect the patient}
Move objects away from the patient to prevent injury.
If the patient's movements are not violent, you may be able to hold their head to prevent it from hitting the ground.
If this is not possible, position bulky clothing under the patient's head.

\triplesubsection{Protect yourself}
Do not attempt to restrain the patient as they may injury you.
A patient will protect their own airway, so do not risk injury by placing your fingers in their mouth.
Further, use of an adjunct may damage their teeth.

\triplesubsection{Consider use of the rescue position}
If the patient's seizure allows it, place the patient in the rescue position to prevent aspiration\index{pulmonary aspiration} of saliva or blood.
You may need to gently hold the patient on their side.

\triplesubsection{Open the airway}
After the seizure has subsided, open the patients airway and check for obstructions.
Return the patient to the rescue position until they regain consciousness.

\triplesubsection{Consider evacuation}
If the patient has a reactive seizure or status epilepticus, they need to be evacuated to advanced medical care.
Otherwise, the patient should be evacuated with a buddy and directed to see a physician for evaluation when they are able.
Patients who have had a seizure are at risk for having a second, and they may remain dazed or confused for hours.
It is not safe for them to stay at an action even if they think that they are fine.

\index{seizures|)}

\section*{Unknown Unresponsive States}

Determining the cause of a patient's unresponsiveness during an action can be challenging.
From context or information from the patient's friends or other observers, you may be able to deduce what has happened to guide treatment.
Treatment should always begin with checking the ABCs.
If there is not a clear mechanism of injury, it is safe to treat for hypoglycemia as described in \fullref{ch:bg_disorders} as this will not cause problems for other illnesses or injuries.\supercite[p. 230]{nols}

\section*{Summary}

Unresponsive states may be identifiable as having known origins such as hypoglycemia or trauma, or they may be epileptic seizures.
Protect the patient's airway and attempt to identify the underlying cause.
Patients who become unresponsive should be evacuated.
If you cannot identify the cause, or if you are unsure whether the evacuation should only be sending the patient home, the patient should be evacuated to advanced medical care.
