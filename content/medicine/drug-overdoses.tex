\chapter{Drug Overdoses}
\label{ch:drug_overdoses}

% TODO could be nice to source this
\epigraph{Tune in, turn on, drop out.}{Timothy Leary}

\index{overdoses|(}
\index{psychoactive drugs|seealso {overdoses}}
\index{psychoactive drugs|(}

\noindent
Psychoactive drugs are found in many anarchist spaces and within left-wing social movements.
They range from the mild like caffeine and nicotine to the ``harder'' drugs like heroin or methamphetamines.
Use of psychoactives can be relaxing, exciting, and in some cases even mind expanding.
This chapter is not about when drug use goes right but rather when it goes wrong.

Use of psychoactives (beyond coffee and cigarettes) occurs during many actions, though as actions tend more militant, their presence generally drops.
Medics will most likely encounter users at demonstrations and especially when actions are more fun and celebratory.
They will rarely, if ever, encounter intentional overdoses during actions.
Most overdoses will be accidental.
Users of psychoactives may overdose due to changing tolerance, varying potency, or poor measurements of dosages.
Interactions between different drugs may lead to overdose, and users may not always be getting the drug they intended.
Regardless of how it happens, medics need to know how to treat an overdose.

Overdoses during actions may be difficult to find in a timely manner.
Fascist and State violence is more predictable and happens in expected locations.
Overdoses tend to happen at actions where there are larger crowds, music, and other loud entertainment.
In order to find patients in the early stages of an overdose, it is recommended that medics patrol through crowds and more actively seek out patients.

\index{psychoactive drugs|)}

\begin{war-story}{An Overdose in the Shadows}{Anonymous}
We were helping out at an anti-gentrification action that had started in the early afternoon and ended up stretching late into the night.
Thousands of students, neighbors, and all sorts of other people had marched through the streets crying out against rising rents and evictions.
It was also a bit of a cultural celebration with music playing from speakers strapped to the roofs of vans.

The march has ended at a park and turned into a giant block party.
People had been drinking and taking drugs the whole day, so we had our two teams walking around looking for people who were sick or passed out.

My team was walking around the poorly lit edges of the celebration when someone saw our reflective vests and flagged us down.
She said her friend ``wasn't doing so good.''
We followed her to her friend who was rolling around, twitching, and grunting at the base of a tree.
I recognized him by his outfit as someone who has been partying all day.

Our initial examination showed that he had an elevated heart rate (132 \acrshort{BPM}), decreased oxygen saturation (87\%), and dilated pupils that did not react to light.
The patient's friend didn't know what he had taken, so we guessed it was an MDMA overdose.
That didn't really matter because all we could do was call \acrshort{EMS}.
While we waited, we moved the patient off the tree's roots and on to the grass and wrapped him in an emergency blanket.
EMS arrived, we gave them our findings, and they took the patient to a hospital.

We don't know what happened to the patient, but we felt lucky that we were dressed as obvious medics that day and decided to walk through the quiet parts of the park.
\end{war-story}

\section*{Serotonin Syndrome}
\index{serotonin syndrome|(}

\index{serotonin|(}
Serotonin is a neurotransmitter that has many effects on the body, though it is commonly associated with simply being a happiness chemical.
Serotonin affects mood, appetite, pain, anxiety, and sleep among other functions.
\index{serotonin|)}

A serotonergic drug is any drug that affects serotonin such as serotonin agonists and antagonists, serotonin reuptake inhibitors, and serotonin releasing agents.
Serotonergic drugs include antidepressants\index{antidepressants} like \acrshort{MAOI}s, \acrshort{SSRI}s, \acrshort{SNRI}s, and cyclic antidepressants; recreational drugs like amphetamines, cocaine, fentanyl, \acrshort{LSD}, and mescaline; and other drugs like St. John's Wort\index{St. John's Wort}, tramadol\index{tramadol}, and lithium.

Serotonin syndrome is a group of symptoms that appear in reaction to taking serotonergic drugs.
It may occur in as a result of a single drug such in which case it often occurs after increasing the dosage of antidepressants.
In the context of overdose and recreational drug use, it typically occurs as a result of an overly large dose or the concomitant use of multiple serotonergic drugs.

\subsection*{Signs and Symptoms}

Serotonin syndrome is characterized by cognitive, autonomic, and neuromuscular effects.
Symptoms may include restlessness, anxiety, altered level of consciousness, agitation, perspiration, \gls{hyperthermia}, tachycardia, tachypnea, pupil dilation (\gls{mydriasis}), muscle rigidity, loss of coordination, hyperreflexia, tremors, and myoclonus (brief, involuntary spasms in a muscle group).
The most common symptoms are altered mental status, hyperthermia, muscle rigidity, and myoclonus.\supercite[p. 1223]{tintinallis}

A diagnosis of serotonin syndrome requires the exclusion of other medical and psychiatric conditions,\supercite[p. 1223-4]{tintinallis} so a conclusive diagnosis and treatment is outside the scope of a riot medic's knowledge.
Medics should be aware of this syndrome to strengthen recommendations for advanced medical care for patients whose recent history and symptoms suggest they have serotonin syndrome.

\subsection*{Treatment}

Serotonin syndrome can be mitigated by ceasing taking the serotonergic drugs, however it can be dangerous to recommend that patients stop taking prescription medication.
All patients should be evacuated to advanced medical care to for additional treatment and monitoring.
In mild cases, the patient may not consent to evacuation.
If the patient is taking prescription serotonergic drugs, they should be directed to promptly contact their physician or psychiatrist to discuss their medication.

\index{serotonin syndrome|)}

\section*{General Treatment for Overdoses}

The following general treatment principles will help with all cases of intoxication or overdose.
These are more conservative measures meant to ensure patient health.
In most settings where drugs are being consumed, most people don't want to make a fuss and involve medical professionals.
They often want to wait it out to see if any problems resolve themselves on their own to avoid ``killing the mood.''
As a medic, if you're involved, there is a good chance it is more serious.
Regardless, it is better to error on the side of caution.

\triplesubsection{Deescalate and protect yourself}
Patients may be fearful, paranoid, aggressive, or combative.
You may need to deescalate the situation to protect yourself and bystanders.
Deescalation also minimizes the chances of police involvement.
Personal safety needs to be prioritized over treatment and deescalation.
If the patient hurts you, you cannot help them.

\triplesubsection{Determine drugs consumed}
As part of the patient assessment, you will have asked what medications they have taken.
Knowing what they took, how they took it, and when they took it can help you and advanced medical care guide treatment.
Ask them about what other drugs they took concomitantly.
If the patient is unreliable or unresponsive, you or your buddy should ask these questions to friends and bystanders.
However, do not prioritize the interview over treatment of the patient.
People may be unwilling to answer questions out of fear that you will inform on them to the police.
You may need to assert that you will not do this and that patient confidentiality\index{patient-caregiver confidentiality}\index{legality!overdoses@in overdoses} would legally prevent you from doing so.\footnotemark[1]

\footnotetext[1]{You may need to check the laws\index{legality} in your region to determine if this protection exists.}

\triplesubsection{Avoid food and drink}
If a patient cannot eat or drink on their own, do not give them food or drink.
During your treatment, if the patient appears to be recovering, consider letting them eat or drink a small amount on their own.
If the patient's condition worsens, additional stomach contents may contribute to aspiration\index{pulmonary aspiration}.
However, if your differential diagnosis cannot rule out hypoglycemia\index{hypoglycemia!drug overdoses!in drug overdoses}, consider administering a small amount of sugar.

\triplesubsection{Do not induce vomiting}
Inducing vomiting can lead to aspiration\index{pulmonary aspiration} and may not have any effect on the amount of drugs absorbed by the body.

\triplesubsection{Place patient in rescue position}
If the patient has a reduced level of consciousness but is still breathing, place them in the rescue position to minimize the chances of aspiration\index{pulmonary aspiration} until they recover or advanced medical care arrives.
Continuously monitor their ABCs.

\triplesubsection{Call advanced medical care}
At an action, you may not be able to spend hours with a patient to monitor their condition.
If the patient is not verbally responsive, call emergency medical services.

\triplesubsection{Calm bystanders}
Drug overdose may cause panic in the patient's friends and bystanders.
This panic may be made worse because they may not be sober either.
Speaking words of reassurance in a calm voice generally is sufficient to reduce panic.
Avoid saying things like ``they will be fine'' and ``everything is ok'' because such phrases may not be true.
Use phrases like ``I'm here to help'' and ``I'm trained for this.''

\triplesubsection{Keep bystanders away from the police}
If \acrshort{EMS} is called, police may also arrive either before or after EMS.
Bystanders who are panicked may want to talk as a way to relieve stress.
They may incriminate themselves, the patient, or others.
Explain this risk to bystanders after you have called \acrshort{EMS}, and assign a free medic or the most sober volunteer to act as caretaker who keeps people from talking to the police.

\section*{Common Drug Overdoses}

Patients may overdose on common drugs like paracetamol or nicotine, but these are less likely to be encountered by riot medics.
This section covers only common recreational drugs.

\subsection*{Alcohol Overdoses}
\index{ethanol|see {alcohol}}
\index{alcohol!intoxication|(}

Alcohol\footnotemark[2] is a psychoactive drug generally consumed for its euphoric effects, to increase sociability, or to blunt feelings.
Alcohol is a \acrshort{CNS} depressant.
Blood alcohol levels peak approximately 30 to 60 minutes after ingestion, and absorption is delayed by the presence of food in the stomach.\supercite[p. 1243]{tintinallis}
Mental impairments begin at relatively low doses and progress from decreased ability to concentrate to decreased respiratory rate and heart rate.

Tolerance to a given amount of alcohol varies by weight and gender due to different ratios of fat and water in the body.
Women of the same weight as men will on the average become more intoxicated when consuming given amount of alcohol.
There is individual variation on tolerance to alcohol, and thus symptoms may vary by individual.
Tolerance can be developed by consuming alcohol leading to less effects for the same blood alcohol content (\acrshort{BAC}).
Years of heavy drinking can lead to reduced tolerance as the liver becomes scarred and cannot metabolize alcohol.

\footnotetext[2]{
An alcohol is member of a general class of organic compounds.
In colloquial terms, it is synonymous with ethanol.
In this chapter (and the rest of this book), the term alcohol means ethanol unless otherwise specified.
}

\subsubsection*{Signs and Symptoms}

Mild\footnotemark[3] alcohol intoxication presents with euphoria, increased sociability, impaired judgement, difficulty concentrating, reduced fine motor control, and flushed skin.
Moderate intoxication presents with delayed reactions, impaired senses, confusion, analgesia, ataxia, dizziness, and vomiting.
Severe intoxication (often called alcohol poisoning) presents with severe ataxia, periods of unconsciousness, anterograde amnesia (``blacking out''), vomiting, decreased respiratory rate, decreased heart rate, pupils that do not respond to light, and coma\index{coma}.
Because alcohol is a vasodilator, severely intoxicated patients may have \gls{hypotension} or be hypothermic\index{hypothermia!alcohol intoxication@in alcohol intoxication}.
Patients may also be hypoglycemic\index{hypoglycemia!alcohol intoxication@in alcohol intoxication} which itself presents similar to drunkenness.
Death from alcohol can be caused by the aspiration of vomit, hypoventilation, or alcohol contributing to an overdose by other drugs.

\footnotetext[3]{
The terms mild, moderate, and severe and not medically precise descriptors of intoxication and are being used in place of \acrshort{BAC}.
Medics cannot easily measure \acrshort{BAC}, and estimation formulas are not worth memorizing as treatment is based on symptoms and not \acrshort{BAC} alone.
Mild, moderate, and severe intoxication are roughly equivalent to the colloquial terms tipsy, drunk, and shitfaced.
}

\subsubsection*{Specific Treatment}

% TODO consider adding research about the immediate administration of activated charcoal, but also needs research as there may be risks

There is nothing that can be done by medics to reverse alcohol intoxication.
Their task is to determine if the patient is sufficiently intoxicated to require advanced medical care, and if not, to monitor the patient and provide basic care.
Alcohol can lead to overdoses with cocaine, barbiturates, benzodiazepines, and opioids among others.
The patient and bystanders should be interviewed to determine if there is any risk due to concomitant drug use.

Treatment for severe alcohol intoxication by the general public is often to let the patient ``sleep it off,'' which is to say no treatment is given.
Urban legends\index{urban legends!alcohol intoxication@in alcohol intoxication} say that getting the patient to eat something, drink water, or drink coffee will help reduce intoxication.
This may make the patient feel better, but it does not reduce \acrshort{BAC}.

\index{alcohol!intoxication|)}

\subsection*{Opioid Overdoses}
\index{opioids|(}

Opioids\footnotemark[4] are natural and synthetic substances that are primarily used for pain relief and anaesthesia.
Common opioids are opium, morphine, codeine, hydrocodone (sold mixed with either paracetamol [Vicodin] or ibuprofen [Vicoprofen]), oxycodone (OxyContin), heroine, fentanyl, and methadone.
Opioids are used recreationally for their euphoric effects and may be administered by injection, smoking, insufflation (snorting), or oral consumption.
Their effects peak about 30 to 60 minutes after consumption when taken orally (90 minutes for controlled release tablets) whereas the effects peak near immediately with other methods.
Opioids' effects can last between 1 to 6 hours depending on the opioid used.

\index{opioids|)}

\footnotetext[4]{
The term opiate is used to describe substances derived from opium.
Opioid is a more modern term that describes all substances that act on opioid receptors.
}

\index{opioid overdose|(}

Tolerance for opiates develops with their repeated use.
Non-standard strength and quality of opiates can lead to poor estimation of dosage resulting in overdose.
For most users, a lethal dose of heroin can be as low as \SI{75}{\mg}.
A lethal dose for fentanyl for most humans is \SI{2}{\mg} (\autoref{fig:fentanyl_lethal_dose}).

Taking opioids concomitant with benzodiazepines and alcohol increases the chance of overdose.

\begin{figure}[htbp]
\centering
\caption{Lethal Dose of Fentanyl\supercite{snailsnail}}
\label{fig:fentanyl_lethal_dose}
% a US penny is 1.905cm tall, so add a teensy bit to account for the 3D-ness of the ridge
\includesvg[height=1.91cm, keepaspectratio]{lethal-dose-of-fentanyl}
\end{figure}

\subsubsection*{Signs and Symptoms}

Effects of opioid use include euphoria, reduction in anxiety, drowsiness, disorientation, delirium, nausea, analgesia, muscle weakness, muscle spasms, seizures, flush and warm skin, miosis (pupil constriction), hypotension, \gls{bradycardia}, and hypoventilation.

\subsubsection*{Specific Treatment}
\index{Narcan|seealso {naloxone}}
\index{naloxone|(}

Naloxone is a treatment option that may be available in your region that requires neither a prescription nor a medical license.
Naloxone works by binding to opioid receptors with higher affinity than opioids but without activating the receptors.
Naloxone can be administered nasally (Narcan) or intramuscularly (Evzio) and reverses overdose within 2 to 3 minutes.
If overdose symptoms show minimal or no improvement, naloxone can be re-administered in \SI{2}{\mg} doses every 2 to 3 minutes up to a maximum of \SI{10}{\mg} until symptoms improve.
If available, medics should use a naloxone nasal spray over an autoinjector.
Nasal sprays can be used multiple times on a single patient, and the devices are smaller.

\index{Narcan|(}

\triplesubsection{Administering Narcan}
Instructions for administering Narcan are listed below for educational purposes.
If you carry naloxone, you should follow your device's specific instructions.

Narcan is a nasal spray that delivers pre-measured \SI{4}{\mg} doses of naloxone (\autoref{fig:narcan_administration}).
Remove the device from its packaging, place it in the patient's nose, and fully depress the plunger.
Only 50\% of the dose is absorbed through the nasal mucosa, so 5 total doses may be administered.

\index{Narcan|)}

\begin{figure}[htbp]
\centering
\caption{Narcan Administration\supercite{snailsnail}}
\label{fig:narcan_administration}
\includesvg[height=5cm, keepaspectratio]{narcan-administration}
\end{figure}

\index{opioids!withdrawal|(}
\triplesubsection{Watch for signs of withdrawal}
Use of naloxone may cause the patient to rapidly go into opioid withdrawal.
Symptoms may include irritability, agitation, muscle pain, sweating, shakiness, opioid cravings, tachycardia, and elevated blood pressure.
\index{opioids!withdrawal|)}

\triplesubsection{Beware of agiatation}
Patients who have taken stimulants concomitant with opioids may have the stimulants' effects dampened by the opioids.
Treatment with naloxone will reverse the effects of the opioids but not the masked stimulants.
This may lead to a suddenly very alert and active patient who only has signs of stimulant intoxication.

\triplesubsection{Consider evacuation}
For that patients who overdose on opioids and are treated with naloxone, it is strongly recommended that they are evacuated to advanced medical care.
The effects naloxone are 30 to 60 minutes but can be as short 20 minutes if the patient has taken large amounts of opioids.\supercite[p. 1253]{tintinallis}
A medic may use discretion and choose to evacuation to advanced medical care if the patient shows significant improvement and the medic is able to spend hours with the patient to monitor for overdose relapse and to re-administer naloxone.

\index{urban legends!naloxone@in naloxone|(}
\triplesubsection{Naloxone urban legends}
Some urban legends say that naloxone does not work on fentanyl overdoses.
This seems to stem from poorly worded headlines and misunderstood articles that were trying to convey that the strength of fentanyl means that it requires additional naloxone to be effective.
Naloxone can be used to reverse fentanyl overdoses.\supercite{narcan-fentanyl-fact-or-fiction, narcan-fentanyl}
\index{urban legends!naloxone@in naloxone|)}

\index{naloxone|)}
\index{opioid overdose|)}

\subsection*{Sedative-Hypnotic Overdoses}
\index{sedative-hypnotics|(}

Sedatives are substances that have a calming effect, and hypnotics are substances that induce sleep.
Collectively they are known as sedative-hypnotics because of their significant overlap.
Colloquially they are known as ``downers.''
Opioids fall into the sedative-hypnotic category, but they were discussed separately because of the possibility of using naloxone to treat opioid overdose.
Alcohol is also classified as a sedative-hypnotic.
Concomitant use of sedative-hypnotics can have dangerous interactions leading to death via respiratory depression.

Other classes of sedative-hypnotics include barbiturates\index{barbiturates}, benzodiazepines\index{benzodiazepines} (``benzos''), general anaesthetics, and antidepressants\index{antidepressants}.
Common sedative-hypnotics are found in \autoref{tab:common_sedative_hypnotics}.

\begin{table}[htbp]
\footnotesize
\caption{Common Sedative-Hypnotics}
\label{tab:common_sedative_hypnotics}
\centering
\begin{tabularx}{\linewidth}{|l|X|}
    \hline
    \multicolumn{1}{|c|}{\textbf{Name}} & \multicolumn{1}{c|}{\textbf{Description}} \\
    \hline
    Phenobarbital
        & A barbiturate; medically used as an anticonvulsant and sleep aid; recreationally used as a euphoric sedative \\
    \hline
    Diazepam\index{diazepam} (Valium\index{Valium|see {diazepam}})
        & A benzodiazepine used to treat anxiety, muscle spasms, and difficulty sleeping \\
    \hline
    \acrshort{GHB}\index{GHB}
        & Medically used as a general anaesthetic; used as a recreational drug with euphoric and ethanol-like effects; used as a date rape drug to induce unconsciousness and memory loss \\
    \hline
    Ketamine\index{ketamine}
        & Medically used as an anaesthetic and to treat depression; recreationally used to induce hallucinations and a dissociative state \\
    \hline
\end{tabularx}
\end{table}

\subsubsection*{Signs and Symptoms}

General shared signs and symptoms across many sedative-hypnotics include euphoria, calmness, loss of coordination, slurred speech, muscle weakness, difficulty concentrating, hypotension, hypoventilation, \gls{bradycardia}, and loss of consciousness.

\subsubsection*{Specific Treatment}

Treatment for sedative-hypnotics is airway management and evacuation to advanced medical care.
\index{naloxone|(}
Naloxone is safe to use on anyone as it has minimal side effects, including those experiencing no overdose at all.
Thus, it is not important to differentiate sedative-hypnotics from opioids in order to treat patients.
Patients who display signs of opioid overdose can be treated with naloxone with no ill effects.
\index{naloxone|)}

\index{sedative-hypnotics|)}

\subsection*{Stimulant Overdoses}
\index{stimulants|(}

Stimulant is a broad classification of drugs that increase \acrshort{CNS} activity or are found to be invigorating.
Colloquially they are known as ``uppers.''
Common stimulants are nicotine and caffeine.
Medical use of stimulants includes treatment for sleep disorders, mood disorders, and impulse control disorders.

Recreational stimulants include methamphetamine\index{methamphetamine}, \acrshort{MDMA}\index{MDMA} (ecstasy, molly), and cocaine\index{cocaine}.
Stimulants are often taken to increase motivation and propensity for activity, for their euphoric effects, and to induce insomnia.
All of these effects can be used to increase the user's ability to continue partying.
Stimulants are sometimes taken concomitantly with sedative-hypnotics to counter the latter's sedating effects.

% TODO it would be nice to turn this into a table
Peak action when insufflating cocaine is 20 to 30 minutes after use with effects lasting 1 to 2 hours.
Effects peak at 3 to 5 minutes when smoked and the effects last about 30 to 60 minutes.
Peak action when insufflating methamphetamine is 1 to 2 hours and 5 to 10 minutes when smoked.
Effects of methamphetamine last 8 to 12 hours.

\index{cocaethylene|(}
Concomitant use of cocaine with alcohol leads to the metabolization of cocaethylene in the liver.
Cocaethylene is similar to cocaine, but increases the risk of sudden death by 18 to 25 times compared to cocaine.\supercite{cocaethylene}
\index{cocaethylene|)}

\index{MDMA|(}
MDMA is a serotonergic drug.
Concomitant use while a patient is taking \acrshort{MAOI}s, \acrshort{SSRI}s, or other antidepressants\index{antidepressants} can lead to serotonin syndrome.
Individuals taking \acrshort{MAOI}s or \acrshort{SSRI}s are strong advised against taking MDMA.
\index{MDMA|)}

\subsubsection*{Signs and Symptoms}

Effects of stimulants may include feelings of happiness, elevated energy levels, agitation, paranoia, the feeling of bugs on or under the skin (formication\index{formication}), sweating, dilated pupils, tachycardia, hypertension, and hyperthermia.
Sweating can lead to dehydration, and replenishing lost water without replenishing salts can lead to low blood sodium concentration (hyponatremia).

\index{psychosis!drug-induced|(}
Users of amphetamines, in particular methamphetamine, may experience psychosis at high doses.
This may present as paranoia, a feeling of persecution, anxiety, delusions, and hallucinations.
\index{psychosis!drug-induced|)}

\index{acute coronary syndrome!cocaine use@in cocaine use|(}
Cocaine use causes vasoconstriction of the coronary arteries which can lead to acute coronary syndrome (\acrshort{ACS}).\supercite[p. 1257]{tintinallis}
The main symptom of \acrshort{ACS} is chest pain or tightness.
The patient may be sweating, have heart palpations, or have nausea.
Risk of \acrshort{ACS} while using cocaine is increased by smoking cigarettes, and its risk is highest in smokers who regularly use cocaine.
For more information on \acrshort{ACS}, see \fullref{ch:respiratory_and_cardiac}.
\index{acute coronary syndrome!cocaine use@in cocaine use|)}

\subsubsection*{Specific Treatment}

Patients who display signs of \acrshort{ACS} or serotonin syndrome should be evacuated to advanced medical care.
Patients who are hyperthermic should be cooled using fanning and water mist.
More complete cooling techniques are discussed in \fullref{ch:heat_illness}.
Treatment may be complicated by drug-induced psychosis.
Even with reassurance, patients may not consent to aid, and attempting to render aid may be dangerous for medics.

\index{stimulants|)}

\subsection*{Hallucinogen Overdoses}
\index{marijuana|see {cannabis}}
\index{hallucinogens|(}

Hallucinogens are drugs that cause hallucinations, altered perceptions, and cause changes in thoughts and consciousness.
As a category, hallucinogens are not distinct.
For example, \acrshort{MDMA} is both a stimulant and a hallucinations, and ketamine is both a sedative-hypnotic and a hallucinogen.
Use of hallucinogens is associated with the hippie culture and psychedelia of the 1960's as well as psychonautics,\footnotemark[5] though their use goes back into ancient human history where they have been used in shamanic and spiritual ceremonies.
Common hallucinogens are cannabis\index{cannabis} (marijuana, weed), \acrshort{LSD}\index{LSD} (acid), psilocybin\index{psilocybin} (mushrooms), DMT, and substituted phenethylamines (such as 2C-B and mescaline).

\footnotetext[5]{
From Greek \textit{psychē} (``spirit/mind'') + \textit{naútēs} (``sailor/navigator''), meaning ``sailor of the soul.''\supercite[p. 434]{hallucinations}
}

Hallucinogens can be smoked, ingested either through baked goods or teas, insufflated, or (less commonly) injected.
The duration and intensity of hallucinations and altered mental state depends on the drug, amount, and method of consumption.
Effects of LSD last for 8 to 12 hours and mescaline for 8 to 14.
Effects of snorting ketamine last 30 to 45 minutes and smoking DMT last for 5 to 20 minutes.

\subsubsection*{Signs and Symptoms}

Typical physical symptoms for hallucinogens include nausea, mydriasis, dry mouth (xerostomia), hyperthermia, \gls{hypothermia}, sweating, tachycardia, and (rarely) seizures.
Mental changes include detachment from reality, visual and auditory hallucinations, delusions, and what may appear to be confusion and a lack of focus.
Generally symptoms are most easily recognized as mydriasis and altered mental state.

\index{psychosis!drug-induced|(}
A bad trip (drug-induced temporary psychosis) is an unpleasant or terrifying experience while using psychoactive drugs and in particular hallucinogens.
Bad trips may be brought on by the user's mood, the setting, or an excessive dose.
Symptoms include fear, anxiety, panic, delusions, a feeling of being trapped, and ego death.
As part of panic, individuals may become erratic and hyperventilate.
\index{psychosis!drug-induced|)}

\subsubsection*{Specific Treatment}

The only treatment may be the mitigation of a bad trip by making the patient feel calm and comfortable.
The ``bad'' part of a trip may be short lived, and calming the patient may allow them to continue on with their trip after treatment.
This may be a long process as communication with the patient may be difficult.
Patients may be agoraphobic or claustrophobic, frightened, and untrusting of those who try to help them.
Generally, move the patient to somewhere with less stimulation away from loud noises and bright lights.
Patients with detachment from reality may find comfort by being instructed to focus on an object that they can identify as permanent.
Consider giving the patient a patient a leaf, flower, shell, ball, or other small object they can hold in their hand and instruct them to focus on it whenever they are unsure what is real or not or whether they are alive or not.

\index{hallucinogens|)}

\section*{Summary}

Medics' main focus on treating overdose is to calm patients and bystanders and determine if patients need advanced medical care either for physiological symptoms or drug induced psychosis.
Individual symptoms such as respiratory arrest or hyperthermia can be managed while waiting for evacuation.
Treatment in the field for bad trips and mild overdoses is often palliative and supportive until the effects naturally wear off.
Medics may be able to reverse opioid overdose with naloxone, but more generally they will not be able to render care for severe overdoses of other drugs.
Like when treating all patients, but especially when treating intoxicated patients, a sense of calm from the medics will help ease patients and reduce anxiety.

\index{overdoses|)}
