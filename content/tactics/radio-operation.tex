\chapter{Radio Operation}
\label{ch:radio_operation}

\epigraph{What is needed is action -- action!}{John Brown}

\index{radios|see {two-way radios}}
\index{two-way radios|(}

\noindent
Use of two-way radios is not typically needed for urban and suburban actions.
However, medics should be familiar with basic radio operation and protocols.
Knowing radio protocols helps ensure clear communication during stressful situations.
Different regions have different protocols for radio operations, so the protocols you use may vary from what is described here.

\section*{Equipment Overview}

Two-way radios are radios that can both transmit and receive a signal.
This is in contrast to the type of radio built into a stereo system that can only receive a signal.
Use of two-way radios is typically regulated at the national level, and making transmissions using certain frequencies or using systems with a longer range (typically anything over several watts of broadcast power) requires licensing.
Discussion of licensed radio operation is outside the scope of this book.

Unlicensed, personal use radios operate in on specified ranges of frequencies.
In the US, \acrlong{FRS} (\acrshort{FRS}) is a radio system used for two-way radios offering 22 channels.
In Europe, LPD433 (low power device \SI{433}{\MHz}) offers 16 channels.

When using a two-way radio, signals are sent and received on the same frequency, and the frequency is shared by all members of a group.
Thus, only one member may talk at a time otherwise the signals interfere with one another.

Devices may offer a feature called selective calling which allows radio operators to communicate with a subset of all users within a channel.
There are different protocols for selective calling, however they do not offer privacy, only a reduction in the amount of traffic an operator receives.
Devices that have selective calling require the operator to select both a channel and a code.
Because selective calling places many groups on a single channel, if two operators on the same channel broadcast at the same time, even if they are using different codes, their signals will interfere with one another.

\begin{figure}[htb]
\centering
\caption{Ear Piece and Microphone\supercite{baedr}}
\includesvg[height=6cm, keepaspectratio]{radio-operator}
\end{figure}

To start a transmission, devices will use either push-to-talk (\acrshort{PTT}) or voice operated exchange (\acrshort{VOX}).
PTT devices require manually pushing a button before the device will switch to transmit mode.
VOX devices will begin transmitting when volume crosses a threshold.
VOX devices should be avoided as they may cut off the beginning of a transmission and may unintentionally transmit in loud environments.

As an additional note, if you are considering the use of radios for communication, it is recommended to use earpieces and microphone that can be clipped to a collar.
This prevents the unnecessary broadcasting of tactical information to bystanders including members of law enforcement or fascists.

\section*{Radio Protocol}

Following an established radio protocol makes communication clear and efficient.
This is important because radio transmission degrades the understandability of human voice, and while operating as a medic, radio communications may be about ongoing emergencies.
Minimizing miscommunication will help with reaching and treating patients.

The main thing to remember, if nothing else, is to communicate using ABC: accuracy, brevity, and clarity.
Other guidelines are:

\begin{itemize}
    \item Do not transmit unless necessary
    \item Do not respond on other's behalf unless there is a clear reason to do so
    \item Do not act as relay unless instructed to do so
    \item Do assume that all transmissions are being actively monitored by police and fascists
\end{itemize}

If radio operators are unnecessarily chatty, transmitting the message ``radio discipline'' is typically enough to remind other operators to keep the channel clear for important traffic.

\subsection*{Voice Techniques}

Speaking should be modified to be more understandable during radio transmissions.

\triplesubsection{Rhythm}
Speak in short sentences with pauses between sentences.
Enunciate each word clearly.

\triplesubsection{Speed}
Speak slightly slower than usual.
This is especially important during emergencies when you may be tempted to rush a message.
Saving a couple of seconds to rush a transmission will cost extra time if the receiver needs the message to be repeated.

\triplesubsection{Volume}
Speak at a normal volume.
Yelling does not make the signal louder for the receiver and may cause distortion.

\triplesubsection{Pitch}
Operators with a deep voice should slightly increase the pitch of their voice as this is more easily understood by receivers.

\subsection*{Microphone Techniques}

Understandability can be improved with proper microphone techniques.

\triplesubsection{Adjust gain}
Devices with adjustable gain should be set so that when speaking at a normal volume, the device produces full modulation.

\triplesubsection{Speak over the microphone}
When transmitting, the microphone should be held next to the mouth at a distance of roughly \SI{5}{\cm} so that the operator speaks over the microphone and not into it.
This prevents certain sounds (like ``p'' or ``b'') from ``popping'' against the microphone.\footnotemark[1]

\footnotetext[1]{
For an example of this, hold your hand directly in front of your mouth.
Say the words ``pat'' and ``bat.''
Feel the burst of air against your hand.
}

\triplesubsection{Wait before speaking}
After depressing the PTT button,  pause for roughly 1 second before speaking.
This allows circuits to begin sending and receiving.

\triplesubsection{Release the button when finished}
Avoid leaving the PTT button depressed when you are not speaking.
If you do, the channel will be blocked for other operators.

\subsection*{Voice Procedure}

There are procedures that help operators effectively communicate and prevent multiple operators from talking over one another.
In aviation, maritime, and other more regulated settings, there are strict procedures.
These work because all operators are assumed to be using the same procedure and to be both trained and experienced.
For medics, less strict and less formal procedures can be used.
This is because the full range of procedures is too complex and covers far more than medics will realistically encounter.

\subsubsection*{Call Signs}

Radio operators should use descriptive rather than unique or numerical call signs.
An example would be ``Medic Team 1'' or ``Medic Tent North.''
At large actions there may be many teams of medics and non-medic personnel sharing a channel.
Numeric or nickname call signs will be impossible to remember.\footnotemark[2]

\footnotetext[2]{
As cool as it may seem to use ``Rogue One''\footnotemark[3] as your call sign, that phrase will be utterly meaningless to other operators.
}

\footnotetext[3]{
I'm putting a footnote within a footnote to say, \textit{Rogue One} was the best Star Wars film.
Whomst amongst us did no identify with a bunch of rag tag rebels saying ``fuck authority'' and running off to die in the fight against space-fascism so that others might live?
}

Call signs should be used to start a transmission beginning with the call sign of the recipient followed by the operator's own call sign.
For example, one would begin a transmission with ``Yellow Team, this is Blue Team.''
This is done to get the attention of the recipient as well as preventing multiple people from responding at the same time leading to channel interference.

\subsubsection*{Procedure Words}

Procedure words (prowords) are used to convey information in a condensed, standard format.
A subset of prowords can be found in \autoref{tab:prowords_general}.

\begin{table}[htbp]
\caption{Common Prowords}
\label{tab:prowords_general}
\centering
\begin{tabularx}{\linewidth}{|l|X|}
    \hline
    \multicolumn{1}{|c|}{\textbf{Proword}} & \multicolumn{1}{c|}{\textbf{Meaning}} \\
    \hline
    \textsc{This Is}       & The transmission is from the operator whose call sign immediately follows. \\
    \hline
    \textsc{Over}          & The current transmission is complete and you may now respond. \\
    \hline
    \textsc{Nothing Heard} & No response was received. \\
    \hline
    \textsc{Roger}         & The previous transmission was received and understood. \\
    \hline
    \textsc{Wilco}         & Short for ``\textsc{Roger}, will comply.'' \\
    \hline
    \textsc{Out}           & The current exchange is complete. No response is necessary. \\
    \hline
    \textsc{Wait}          & I am pausing for a few seconds, and you should not transmit. \\
    \hline
    \textsc{Wait -- Out}   & I am pausing, and you may transmit. \\
    \hline
    \textsc{Speak Slower}  & Your transmission was too fast to be understood, and I need you to speak slower. \\
    \hline
    \textsc{Say Again}     & I need you to repeat you last transmission. \\
    \hline
    \textsc{I Say Again}   & I am repeating may last transmission or the portion you requested. \\
    \hline
    \textsc{Read Back}     & Read my entire transmission back to me. \\
    \hline
    \textsc{I Read Back}   & I am reading your entire transmission back to you. \\
    \hline
    \textsc{I Spell}       & The next words will be spoken phonetically. \\
    \hline
    \textsc{Correction}    & I am correcting an error I made in this current transmission. \\
    \hline
    \textsc{Correct}       & Yes / affirmative. Use of ``affirmative'' as a proword should be avoided due to confusion with ``negative.'' \\
    \hline
    \textsc{Negative}      & No / negative. \\
    \hline
    \textsc{Wrong}         & Your previous transmission was wrong, and I am issuing a correction. \\
    \hline
    \textsc{Switch To}     & Switch to the following channel after confirmation. \\
    \hline
    \textsc{Switching To}  & I am confirming a switch to the following channel. \\
    \hline
    \textsc{Radio Check}   & I am requesting a radio check. \\
    \hline
\end{tabularx}
\end{table}

Use of the proword \textsc{Over} is unnecessary if the radios being used issue a tone when the PTT button is released.
Use of the prowords \textsc{Over} and \textsc{Out} may be unnecessary when there are few operators as they can make use of the radio seem overly formal and militaristic.

When a radio check is issued, the operator is requesting that receiving operators report their signal strength and readability of their message.
A radio check can be used to ensure all operators in an area are reachable as well as ensuring that equipment is functioning correctly.
When responding to a radio check, there are prowords for signal strength (\autoref{tab:prowords_signal_strength}) and readability (\autoref{tab:prowords_readability}).
The five values for signal strength correspond to the five bars used to measure cellular network or wifi strength.\footnotemark[4]

\footnotetext[4]{
Those who enjoyed the video game \textit{StarCraft} or the movie \textit{Aliens} will recall the prowords \textsc{Five-By-Five} when describing a transmission.
This corresponds to the prowords \textsc{Loud And Clear}.
}

Note that simple radios may not report signal strength, so reporting signal strength during a radio check may not make sense.
Also note that digital radios have a signal drop off cliff where readability quickly goes from clear to unintelligible.
This is in comparison to analog radios whose readability degrades gradually with distance.

\begin{table}[htbp]
\caption{Signal Strength Prowords}
\label{tab:prowords_signal_strength}
\centering
\begin{tabularx}{\linewidth}{|l|X|}
    \hline
    \multicolumn{1}{|c|}{\textbf{Proword}} & \multicolumn{1}{c|}{\textbf{Meaning}} \\
    \hline
    \textsc{Loud}      & Your signal strength is strong. \\
    \hline
    \textsc{Good}      & Your signal strength is good. \\
    \hline
    \textsc{Weak}      & Your signal strength is weak. \\
    \hline
    \textsc{Very Weak} & Your signal stressful is very weak. \\
    \hline
    \textsc{Fading}    & Your signal strength fades, and your reception cannot be relied upon. \\
    \hline
\end{tabularx}
\end{table}

\begin{table}[htbp]
\caption{Readability Prowords}
\label{tab:prowords_readability}
\centering
\begin{tabularx}{\linewidth}{|l|X|}
    \hline
    \multicolumn{1}{|c|}{\textbf{Proword}} & \multicolumn{1}{c|}{\textbf{Meaning}} \\
    \hline
    \textsc{Clear}             & The quality of your transmission is excellent. \\
    \hline
    \textsc{Readable}          & The quality of your transmission is satisfactory. \\
    \hline
    \textsc{Distorted}         & I am having trouble reading you due to distortion. \\
    \hline
    \textsc{With Interference} & I am having trouble reading you due to interference. \\
    \hline
    \textsc{Intermittent}      & I am having trouble reading you due to intermittent signal loss. \\
    \hline
    \textsc{Unreadable}        & The quality if your transmission is so poor that you cannot be read. \\
    \hline
\end{tabularx}
\end{table}

When responding to a \textsc{Radio Check}, an example response would be \textsc{Loud And Clear} for high quality signal and a readable transmission.
For a weak signal with interference, the prowords would be \textsc{Weak With Interference}.

\subsection*{NATO Phonetic Alphabet}

The \acrshort{NATO} Phonetic Alphabet (\autoref{tab:nato_alphabet}) is an acrophonetic mapping of alphabet letters to words to be used for clear communication and spelling over radios.
For example, the word ``medic'' would be spelled ``\textbf{M}ike-\textbf{E}cho-\textbf{D}elta-\textbf{I}ndia-\textbf{C}harlie.''
The NATO phonetic alphabet is a standard to make it easier for radio operators to quickly spell words and listen for spellings.
The words in the list were chosen to be audibly distinct.
This standard also prevents radio operators from using their own mapping of letters which can be difficult to do on the fly and may not be clear over a weak or static filled signal.

\begin{table}[htbp]
\caption{NATO Phonetic Alphabet\supercite{nato-alphabet}}
\label{tab:nato_alphabet}
\centering
\begin{tabular}[c]{|c|l|l|l|}
\hline
    \textbf{Symbol} &
        \multicolumn{1}{|c|}{\textbf{Code Word}} &
        \multicolumn{1}{|c|}{\textbf{Pronunciation}} &
        \multicolumn{1}{|c|}{\textbf{Alt.}} \\
    \hline
    A & Alpha    & AL-fah       &             \\
    B & Bravo    & BRAH-voh     &             \\
    C & Charlie  & CHAR-lee     & SHAR-lee    \\
    D & Delta    & DELL-tah     &             \\
    E & Echo     & ECK-oh       &             \\
    F & Foxtrot  & FOKS-trot    &             \\
    G & Golf     & GOLF         &             \\
    H & Hotel    & HO-tell      &             \\
    I & India    & IN-dee-ah    &             \\
    J & Juliett  & JEW-lee-ETT  &             \\
    K & Kilo     & KEY-loh      &             \\
    L & Lima     & LEE-mah      &             \\
    M & Mike     & MIKE         &             \\
    N & November & no-VEM-ber   &             \\
    O & Oscar    & OSS-cah      &             \\
    P & Papa     & pah-PAH      &             \\
    Q & Quebec   & KEH-beck     &             \\
    R & Romeo    & ROW-me-oh    &             \\
    S & Sierra   & see-AIR-ah   &             \\
    T & Tango    & TANG-go      &             \\
    U & Uniform  & YOU-nee-form & OO-nee-form \\
    V & Victor   & VIK-tah      &             \\
    W & Whiskey  & WISS-key     &             \\
    X & X-ray    & ECKS-ray     &             \\
    Y & Yankee   & YANG-kee     &             \\
    Z & Zulu     & ZOO-loo      &             \\
    0 & Zero     & ZE-ro        &             \\
    1 & One      & WUN          &             \\
    2 & Two      & TOO          &             \\
    3 & Three    & TREE         &             \\
    4 & Four     & FOW-er       &             \\
    5 & Five     & FIFE         &             \\
    6 & Six      & SIX          &             \\
    7 & Seven    & SEV-en       &             \\
    8 & Eight    & AIT          &             \\
    9 & Nine     & NIN-er       &             \\
    \hline
\end{tabular}
\end{table}

\subsection*{Example Conversations}

Consider the following action.
There are two roaming medic teams, a medic tent, and an operations team tasked with organizing equipment and live music.
Respectively, their calls signs are Medic Team 1, Medic Team 2, Medic Tent, and Operations.

\subsubsection*{Example 1}

Medic Team 1 has found several injured individuals.
They require assistance from Medic Team 2.

\addvbuffer[12pt]{
\noindent
\centering
\begin{tabularx}{0.9\textwidth}{@{}l X}
    Medic Team 1: & Medic Team 2, \textsc{This Is} Medic Team 1. \\
    Medic Team 2: & Medic Team 1, \textsc{This Is} Medic Team 2. \\
    Medic Team 1: & We have [\textit{unintelligible}] at the end of [\textit{unintelligible}]. \\
    Medic Team 2: & Medic Team 1. \textsc{Say Again.} \\
    Medic Team 1: & We have multiple injured patients at the end of Oat Street. Requesting assistance. \\
    Medic Team 2: & Is that Oat Street or Oak Street? \\
    Medic Team 1: & Oat Street. \textsc{I Spell Oscar-Alpha-Tango}. \\
    Medic Team 2: & \textsc{Roger.} On our way. \textsc{Out.} \\
\end{tabularx}
}

During this exchange, for clarity, \textsc{Medic Team 2} requests that \textsc{Medic Team 1} repeats themself.
After doing so, they additionally spell out a word to prevent confusion.

\subsubsection*{Example 2}

Medic Team 1 has walked over a kilometer from the main action with a small group but wants to check that they are still reachable by all other operators.

\addvbuffer[12pt]{
\noindent
\centering
\begin{tabularx}{0.9\textwidth}{@{}l X}
    Medic Team 1: & Calling everyone. \textsc{This Is} Medic Team 1. \textsc{Radio Check.} \\
    Operations:   & Medic Team 1, \textsc{This Is} Operations. Your signal is \textsc{Loud And Clear.} \\
    Medic Tent:   & Medic Team 1, \textsc{This Is} Medic Tent. Your signal is \textsc{Loud And Good.} \\
    Medic Team 2: & Medic Team 1, \textsc{This Is} Medic Team 2. Your signal is \textsc{Weak With Interference.} \\
\end{tabularx}
}

Each team that receives the transmission responds with details about transmission quality.
In this example, it is assumed that each team's radio has a readout for signal strength.

\subsubsection*{Example 3}

Operations wants to plan logistics for cleaning up the medic tent at the end of the action, but they want to leave the main channel clear for the other medics to communicate.

\addvbuffer[12pt]{
\noindent
\centering
\begin{tabularx}{0.9\textwidth}{@{}l X}
    Operations: & Medic Tent, \textsc{This Is} Operations. \\
    Medic Tent: & Operations, \textsc{This Is} Medic Tent. \\
    Operations: & \textsc{Switch To} channel 4. \\
    Medic Tent: & \textsc{Switching To} channel 4. \\
\end{tabularx}
}

When requesting a channel switch, wait for confirmation from the other operator before switching channels to prevent miscommunication and confusion.

\section*{Summary}

Even without following any procedures, it is easy to effectively communicate using radios if the operators remember to be accurate, brief, and clear.
Following procedures becomes more necessary during larger actions.
Remember to say who you are addressing, who you are, and use short transmissions.
Lastly, do not forget that the State or fascists may be snooping on your transmissions.
Use caution.

\index{two-way radios|)}
